SRC=main.c lex.c gen.c log.c ast.c
BIN=bin/0xHYCC
ARGS=-Wextra -Wall -pedantic

main: setup
	gcc -O2 $(ARGS) $(SRC) -o $(BIN)

debug: setup
	gcc -g $(ARGS) $(SRC) -o $(BIN) -DZ0_DEBUG

setup:
	mkdir -p bin

compile: 
	$(BIN) ./test/փորձ_$(T_NO).հյ

assemble: compile
	gcc -o ./bin/a.out ./bin/hycc.s -no-pie

run: assemble
	./bin/a.out

clear:
	rm -rf ./bin
