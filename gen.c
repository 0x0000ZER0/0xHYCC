#include "defs.h"

#include <string.h>
#include <stdio.h>
#include <wchar.h>
#include <malloc.h>

// ================ DECL ================

static u32
asm_ast_gen(env*, node*, u32, a_type);

// ================ IMPL ================

void
asm_init(env *ctx) {
	ctx->g_cap = 256;

	ctx->g_name = malloc(sizeof (s32*) * ctx->g_cap);
	if (ctx->g_name == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել ընդհանուր անունների աղյուսակի համար");
	ctx->g_ptype = malloc(sizeof (p_type) * ctx->g_cap);
	if (ctx->g_ptype == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել ընդհանուր հասարակ տիպերի աղյուսակի համար");
	ctx->g_stype = malloc(sizeof (s_type) * ctx->g_cap);
	if (ctx->g_stype == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել ընդհանուր կառուցվածքային տիպերի աղյուսակի համար");
	ctx->g_els = malloc(sizeof (u32) * ctx->g_cap);
	if (ctx->g_els == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել քանակի աղյուսակի համար");
	ctx->g_ctype = malloc(sizeof (c_type) * ctx->g_cap);
	if (ctx->g_ctype == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել հիշողության տեսակների աղյուսակի համար");
	ctx->g_pos = malloc(sizeof (u32) * ctx->g_cap);
	if (ctx->g_ctype == NULL)
		log_err(L"չհաջողվեց հիշողություն հատկացնել լոկալ դիրքերի աղյուսակի համար");

	// ============== ՀԱՏՈՒԿ ԱՆՈՒՆՆԵՐ ==============
	ctx->g_name[0]  = wcsdup(L"տպիր");
	ctx->g_ptype[0] = P_VOID;
	ctx->g_stype[0] = S_FUNC;
	ctx->g_ctype[0] = C_GLOBAL;
	ctx->g_els[0]   = 1;
	// ============== ՀԱՏՈՒԿ ԱՆՈՒՆՆԵՐ ==============
	ctx->g_len = 1;

	ctx->r_free[0] = true;
	ctx->r_free[1] = true;
	ctx->r_free[2] = true;
	ctx->r_free[3] = true;

	ctx->r_name64[0] = (u8*)strdup("r10");
	ctx->r_name64[1] = (u8*)strdup("r11");
	ctx->r_name64[2] = (u8*)strdup("r12");
	ctx->r_name64[3] = (u8*)strdup("r13");
	ctx->r_name64[4] = (u8*)strdup("r9");
	ctx->r_name64[5] = (u8*)strdup("r8");
	ctx->r_name64[6] = (u8*)strdup("rcx");
	ctx->r_name64[7] = (u8*)strdup("rdx");
	ctx->r_name64[8] = (u8*)strdup("rsi");
	ctx->r_name64[9] = (u8*)strdup("rdi");
	
	ctx->r_name32[0] = (u8*)strdup("r10d");
	ctx->r_name32[1] = (u8*)strdup("r11d");
	ctx->r_name32[2] = (u8*)strdup("r12d");
	ctx->r_name32[3] = (u8*)strdup("r13d");
	ctx->r_name32[4] = (u8*)strdup("r9d");
	ctx->r_name32[5] = (u8*)strdup("r8d");
	ctx->r_name32[6] = (u8*)strdup("ecx");
	ctx->r_name32[7] = (u8*)strdup("edx");
	ctx->r_name32[8] = (u8*)strdup("esi");
	ctx->r_name32[9] = (u8*)strdup("edi");

	ctx->r_name16[0] = (u8*)strdup("r10w");
	ctx->r_name16[1] = (u8*)strdup("r11w");
	ctx->r_name16[2] = (u8*)strdup("r12w");
	ctx->r_name16[3] = (u8*)strdup("r13w");
	ctx->r_name16[4] = (u8*)strdup("r9w");
	ctx->r_name16[5] = (u8*)strdup("r8w");
	ctx->r_name16[6] = (u8*)strdup("cx");
	ctx->r_name16[7] = (u8*)strdup("dx");
	ctx->r_name16[8] = (u8*)strdup("si");
	ctx->r_name16[9] = (u8*)strdup("di");

	ctx->r_name8[0]  = (u8*)strdup("r10b");
	ctx->r_name8[1]  = (u8*)strdup("r11b");
	ctx->r_name8[2]  = (u8*)strdup("r12b");
	ctx->r_name8[3]  = (u8*)strdup("r13b");
	ctx->r_name8[4]  = (u8*)strdup("r9b");
	ctx->r_name8[5]  = (u8*)strdup("r8b");
	ctx->r_name8[6]  = (u8*)strdup("cl");
	ctx->r_name8[7]  = (u8*)strdup("dl");
	ctx->r_name8[8]  = (u8*)strdup("sil");
	ctx->r_name8[9]  = (u8*)strdup("dil");

	ctx->out = fopen("./bin/hycc.s", "w");
	if (ctx->out == NULL)
		log_err(L"չհաջողվեց բացել ելքային ֆայլը");

#ifdef Z0_DEBUG
	log_info(L"ստեղծվեց «asm»֊ն");
#endif
}

u32
asm_item_add(env *ctx, p_type prim, s_type stct, u32 qty, c_type stor) {
	if (ctx->g_len == ctx->g_cap)
		log_err(L"չհաջողվեց հատկացնել տարածք անվան համար");

	u32 id;
	id = ctx->g_len;

	ctx->g_name[id]  = wcsdup(ctx->word);
	ctx->g_ptype[id] = prim;
	ctx->g_stype[id] = stct;
	ctx->g_els[id]   = qty;
	ctx->g_ctype[id] = stor;
	ctx->g_pos[id] 	 = 0;

	++ctx->g_len;
	return id;
}

u32
asm_item_find(env *ctx, c_type stor) {	
	s32 cmp;
	for (u32 i = 0; i < ctx->g_len; ++i) {
		cmp = wcscmp(ctx->word, ctx->g_name[i]);
		if (cmp == 0 && (ctx->g_ctype[i] & stor)) {
#ifdef Z0_DEBUG
			log_info(L"գտնվեց այդ անունը աղյուսակում");
#endif
			return i;
		}
	}

#ifdef Z0_DEBUG
	log_info(L"չգտնվեց այդ անունը աղյուսակում");
#endif
	return ctx->g_cap;
}

u32
asm_type_size(p_type prim) {
	static u32 S[] = {
		0, // P_NONE
		0, // P_VOID
		1, // P_U8
		2, // P_U16
		4, // P_U32
		8, // P_U64
		1, // P_S8
		2, // P_S16
		4, // P_S32
		8, // P_S64
		8, // P_VOID_PTR
		8, // P_U8_PTR
		8, // P_U16_PTR
		8, // P_U32_PTR
		8, // P_U64_PTR
		8, // P_S8_PTR
		8, // P_S16_PTR
		8, // P_S32_PTR
		8, // P_S64_PTR
	};

	return S[prim];
}

static u32
asm_new_reg(env *ctx) {
	for (u32 i = 0; i < 4; ++i) {
		if (ctx->r_free[i]) {
			ctx->r_free[i] = false;		

			return i;
		}
	}

	log_err(L"չհաջողվեց վերցնել ազատ «reg»");
	return -1;
}

static void
asm_free_regs(env *ctx) {
	ctx->r_free[0] = true;
	ctx->r_free[1] = true;
	ctx->r_free[2] = true;
	ctx->r_free[3] = true;
}

static void
asm_free_reg(env *ctx, u32 id) {
	if (ctx->r_free[id])
		log_err(L"նշված «reg»-ը արդեն ազատ է");

	ctx->r_free[id] = true;
}

static void
asm_glob(env *ctx, u32 id) {
	u32 size;
	size = asm_type_size(ctx->g_ptype[id]);

	fwprintf(ctx->out, L"\t .data \n");
	fwprintf(ctx->out, L"\t .globl %ls \n", ctx->g_name[id]);
	fwprintf(ctx->out, L"%ls: \n", ctx->g_name[id]);

	for (u32 i = 0; i < ctx->g_els[id]; ++i) {
		switch (size) {
		case 1:
			fwprintf(ctx->out, L"\t .byte \t 0 \n");
			break;
		case 2:
			fwprintf(ctx->out, L"\t .word \t 0 \n");
			break;
		case 4:
			fwprintf(ctx->out, L"\t .long \t 0 \n");
			break;
		case 8:
			fwprintf(ctx->out, L"\t .quad \t 0 \n");
			break;
		default:
			log_err(L"սխալ չափի փոփոխականը չի կարող գեներացվել");
		}
	}
}

static u32
asm_locl_store(env *ctx, u32 gid, u32 rid) {
	switch (ctx->g_ptype[gid]) {
	case P_U8:
	case P_S8:
		fwprintf(ctx->out, L"\t mov  \t BYTE PTR [rbp - %u], %s \n", ctx->g_pos[gid], ctx->r_name8[rid]);
		break;                                                               
	case P_U16:                                                                  
	case P_S16:                                                                  
		fwprintf(ctx->out, L"\t mov  \t WORD PTR [rbp - %u], %s \n", ctx->g_pos[gid], ctx->r_name16[rid]);
		break;                                                               
	case P_U32:                                                                  
	case P_S32:                                                                  
		fwprintf(ctx->out, L"\t mov  \t DWORD PTR [rbp - %u], %s \n", ctx->g_pos[gid], ctx->r_name32[rid]);
		break;	
	case P_U64:
	case P_S64:
	case P_U8_PTR:
	case P_U16_PTR:
	case P_U32_PTR:
	case P_U64_PTR:
	case P_S8_PTR:
	case P_S16_PTR:
	case P_S32_PTR:
	case P_S64_PTR:
		fwprintf(ctx->out, L"\t mov  \t QWORD PTR [rbp - %u], %s \n", ctx->g_pos[gid], ctx->r_name64[rid]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում «reg»-ը լոկալ փոփոխականում պահելու ժամանակ");
	}

	return rid;
}

static u32
asm_glob_store(env *ctx, u32 gid, u32 rid) {
	switch (ctx->g_ptype[gid]) {
	case P_U8:
	case P_S8:
		fwprintf(ctx->out, L"\t mov  \t BYTE PTR %ls[rip], %s \n", ctx->g_name[gid], ctx->r_name8[rid]);
		break;
	case P_U16:
	case P_S16:
		fwprintf(ctx->out, L"\t mov  \t WORD PTR %ls[rip], %s \n", ctx->g_name[gid], ctx->r_name16[rid]);
		break;
	case P_U32:
	case P_S32:
		fwprintf(ctx->out, L"\t mov  \t DWORD PTR %ls[rip], %s \n", ctx->g_name[gid], ctx->r_name32[rid]);
		break;	
	case P_U64:
	case P_S64:
	case P_U8_PTR:
	case P_U16_PTR:
	case P_U32_PTR:
	case P_U64_PTR:
	case P_S8_PTR:
	case P_S16_PTR:
	case P_S32_PTR:
	case P_S64_PTR:
		fwprintf(ctx->out, L"\t mov  \t QWORD PTR %ls[rip], %s \n", ctx->g_name[gid], ctx->r_name64[rid]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում «reg»-ը գլոբալ փոփոխականում պահելու ժամանակ");
	}

	return rid;
}

static u32
asm_ptr_store(env *ctx, u32 lid, u32 rid, p_type prim) {
	switch (prim) {
	case P_U8:
	case P_S8:
		fwprintf(ctx->out, L"\t mov  \t BYTE PTR [%s], %s \n", ctx->r_name8[lid], ctx->r_name8[rid]);
		break;
	case P_U16:
	case P_S16:
		fwprintf(ctx->out, L"\t mov  \t WORD PTR [%s], %s \n", ctx->r_name16[lid], ctx->r_name16[rid]);
		break;
	case P_U32:
	case P_S32:
		fwprintf(ctx->out, L"\t mov  \t DWORD PTR [%s], %s \n", ctx->r_name32[lid], ctx->r_name32[rid]);
		break;	
	case P_U64:
	case P_S64:
		fwprintf(ctx->out, L"\t mov  \t QWORD PTR [%s], %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում «reg»-ը ցուցիչում պահելու ժամանակ");
	}

	return rid;
}

static u32
asm_locl_load(env *ctx, u32 gid, a_type type) {
	static s32 S[] = { L'-', L'+' };
	s32 sign;
	sign = S[ctx->g_ctype[gid] - 1];

	u32 rid;
	rid = asm_new_reg(ctx);

	switch (ctx->g_ptype[gid]) {
	case P_U8:
	case P_S8:
	        if (type == A_PRE_INC)
	                fwprintf(ctx->out, L"\t inc \t BYTE PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_PRE_DEC)
	                fwprintf(ctx->out, L"\t dec \t BYTE PTR [rbp %lc %u] \n", sign, ctx->g_name[gid]);
	        fwprintf(ctx->out, L"\t movsx\t %s, BYTE PTR [rbp %lc %u] \n", ctx->r_name64[rid], sign, ctx->g_pos[gid]);
	        if (type == A_POST_INC)
	                fwprintf(ctx->out, L"\t inc \t BYTE PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_POST_DEC)
	                fwprintf(ctx->out, L"\t dec \t BYTE PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        break;
	case P_U16:
	case P_S16:
	        if (type == A_PRE_INC)
	                fwprintf(ctx->out, L"\t inc \t WORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_PRE_DEC)
	                fwprintf(ctx->out, L"\t dec \t WORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        fwprintf(ctx->out, L"\t movsx\t %s, WORD PTR [rbp %lc %u] \n", ctx->r_name64[rid], sign, ctx->g_pos[gid]);
	        if (type == A_POST_INC)
	                fwprintf(ctx->out, L"\t inc \t WORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_POST_DEC)
	                fwprintf(ctx->out, L"\t dec \t WORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        break;
	case P_U32:
	case P_S32:
	        if (type == A_PRE_INC)
	                fwprintf(ctx->out, L"\t inc \t DWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_PRE_DEC)
	                fwprintf(ctx->out, L"\t dec \t DWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        fwprintf(ctx->out, L"\t movsx\t %s, DWORD PTR [rbp %lc %u] \n", ctx->r_name64[rid], sign, ctx->g_pos[gid]);
	        if (type == A_POST_INC)
	                fwprintf(ctx->out, L"\t inc \t DWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        if (type == A_POST_DEC)
	                fwprintf(ctx->out, L"\t dec \t DWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        break;
	case P_U64:
	case P_S64:
	case P_S32_PTR:
	case P_S64_PTR:
	        if (type == A_PRE_INC)
	                fwprintf(ctx->out, L"\t inc \t QWORD PTR [rbp %lc %u] \n", sign, ctx->g_name[gid]);
	        if (type == A_PRE_DEC)
	                fwprintf(ctx->out, L"\t dec \t QWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
	        fwprintf(ctx->out, L"\t mov  \t %s, QWORD PTR [rbp %lc %u] \n", ctx->r_name64[rid], sign, ctx->g_pos[gid]);
		if (type == A_POST_INC)
			fwprintf(ctx->out, L"\t inc \t QWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
		if (type == A_POST_DEC)
			fwprintf(ctx->out, L"\t dec \t QWORD PTR [rbp %lc %u] \n", sign, ctx->g_pos[gid]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում լոկալ փոփոխականը «reg»-ում պահելու ժամանակ");
	}

	return rid;
}

static u32
asm_glob_load(env *ctx, u32 gid, a_type type) {
	u32 rid;
	rid = asm_new_reg(ctx);

	switch (ctx->g_ptype[gid]) {
	case P_U8:
	case P_S8:
		if (type == A_PRE_INC)
			fwprintf(ctx->out, L"\t inc \t BYTE PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_PRE_DEC)
			fwprintf(ctx->out, L"\t dec \t BYTE PTR %ls[rip] \n", ctx->g_name[gid]);
		fwprintf(ctx->out, L"\t movsx\t %s, BYTE PTR %ls[rip] \n", ctx->r_name64[rid], ctx->g_name[gid]);
		if (type == A_POST_INC)
			fwprintf(ctx->out, L"\t inc \t BYTE PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_POST_DEC)
			fwprintf(ctx->out, L"\t dec \t BYTE PTR %ls[rip] \n", ctx->g_name[gid]);
		break;
	case P_U16:
	case P_S16:
		if (type == A_PRE_INC)
			fwprintf(ctx->out, L"\t inc \t WORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_PRE_DEC)
			fwprintf(ctx->out, L"\t dec \t WORD PTR %ls[rip] \n", ctx->g_name[gid]);
		fwprintf(ctx->out, L"\t movsx\t %s, WORD PTR %ls[rip] \n", ctx->r_name64[rid], ctx->g_name[gid]);
		if (type == A_POST_INC)
			fwprintf(ctx->out, L"\t inc \t WORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_POST_DEC)
			fwprintf(ctx->out, L"\t dec \t WORD PTR %ls[rip] \n", ctx->g_name[gid]);
		break;
	case P_U32:
	case P_S32:
		if (type == A_PRE_INC)
			fwprintf(ctx->out, L"\t inc \t DWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_PRE_DEC)
			fwprintf(ctx->out, L"\t dec \t DWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		fwprintf(ctx->out, L"\t movsx\t %s, DWORD PTR %ls[rip] \n", ctx->r_name64[rid], ctx->g_name[gid]);
		if (type == A_POST_INC)
			fwprintf(ctx->out, L"\t inc \t DWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_POST_DEC)
			fwprintf(ctx->out, L"\t dec \t DWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		break;
	case P_U64:
	case P_S64:
	case P_U8_PTR:
	case P_U16_PTR:
	case P_U32_PTR:
	case P_U64_PTR:
	case P_S8_PTR:
	case P_S16_PTR:
	case P_S32_PTR:
	case P_S64_PTR:
		if (type == A_PRE_INC)
			fwprintf(ctx->out, L"\t inc \t QWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_PRE_DEC)
			fwprintf(ctx->out, L"\t dec \t QWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		fwprintf(ctx->out, L"\t mov  \t %s, QWORD PTR %ls[rip] \n", ctx->r_name64[rid], ctx->g_name[gid]);
		if (type == A_POST_INC)
			fwprintf(ctx->out, L"\t inc \t QWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		if (type == A_POST_DEC)
			fwprintf(ctx->out, L"\t dec \t QWORD PTR %ls[rip] \n", ctx->g_name[gid]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում գլոբալ փոփոխականը «reg»-ում պահելու ժամանակ");
	}

	return rid;
}

static u32
asm_cmp_set(env *ctx, u32 lid, u32 rid, a_type type) {
	static s32 *S[] = {
		L"setl",  L"setg",
		L"setle", L"setge",
		L"sete",  L"setne"
	};	

	fwprintf(ctx->out, L"\t cmp  \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	fwprintf(ctx->out, L"\t %ls \t %s \n", S[type - A_LT], ctx->r_name8[lid]);
	fwprintf(ctx->out, L"\t movzx\t %s, %s \n", ctx->r_name64[lid], ctx->r_name8[lid]);

	asm_free_reg(ctx, rid);
	return lid;
}

static void
asm_lbl(env *ctx, u32 lbl) {
	fwprintf(ctx->out, L"L%u: \n", lbl);
}

static u32
asm_lbl_no() {
	static u32 lbl = 0;
	u32 t;
	t = lbl;
	
	++lbl;
	return t;
}

u32
asm_glob_str(env *ctx) {
	static u32 lbl = 0;
	u32 tmp;
	tmp = lbl;
	++lbl;

	fwprintf(ctx->out, L"STR%u: \n", tmp);

	s32 *ptr;
	ptr = ctx->word;
	while (*ptr++)
		fwprintf(ctx->out, L"\t .long \t %i \n", *ptr);

	return tmp;
}

static u32
asm_load_str(env *ctx, u32 lbl) {
	u32 id;
	id = asm_new_reg(ctx);
	fwprintf(ctx->out, L"\t mov  \t %s, OFFSET FLAT:STR%u \n", ctx->r_name32[id], lbl);

	return id;
}

static void
asm_jmp(env *ctx, u32 lbl) {
	fwprintf(ctx->out, L"\t jmp  \t L%u \n", lbl);
}

static u32
asm_mov_int(env *ctx, s32 val) {
	u32 id;
	id = asm_new_reg(ctx);

	fwprintf(ctx->out, L"\t mov  \t %s, %i \n", ctx->r_name64[id], val);

	return id;
}

static u32
asm_addr(env *ctx, u32 gid) {
	u32 id;
	id = asm_new_reg(ctx);

	fwprintf(ctx->out, L"\t lea  \t %s, %ls[rip] \n", ctx->r_name64[id], ctx->g_name[gid]);

	return id;
}

static u32
asm_dref(env *ctx, u32 id, p_type prim) {
	switch (prim) {
	case P_U8_PTR:
	case P_S8_PTR:
		fwprintf(ctx->out, L"\t movsx\t %s, BYTE PTR [%s] \n", ctx->r_name64[id], ctx->r_name64[id]);
		break;
	case P_U16_PTR:
	case P_S16_PTR:
		fwprintf(ctx->out, L"\t movsx\t %s, WORD PTR [%s] \n", ctx->r_name64[id], ctx->r_name64[id]);
		break;
	case P_U32_PTR:
	case P_S32_PTR:
		fwprintf(ctx->out, L"\t movsx\t %s, DWORD PTR [%s] \n", ctx->r_name64[id], ctx->r_name64[id]);
		break;
	case P_U64_PTR:
	case P_S64_PTR:
		fwprintf(ctx->out, L"\t mov  \t %s, QWORD PTR [%s] \n", ctx->r_name64[id], ctx->r_name64[id]);
		break;
	default:
		log_err(L"չճանաչված տիպ, ցուցիչի արժեքը ստանալու գեներացման ժամանակ");
	}	

	return id;
}

static u32
asm_add(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t add  \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);	

	return lid;
}

static u32
asm_sub(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t sub  \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);

	return lid;
}

static u32
asm_mul(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t imul \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);

	return lid;
}

static u32
asm_div(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t mov  \t rax, %s \n", ctx->r_name64[lid]);
	fwprintf(ctx->out, L"\t cqo  \n");
	fwprintf(ctx->out, L"\t idiv \t %s \n", ctx->r_name64[rid]);
	fwprintf(ctx->out, L"\t mov  \t %s, rax \n", ctx->r_name64[lid]);
	asm_free_reg(ctx, rid);

	return lid;
}

static u32
asm_b_and(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t and  \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);
		
	return lid;
}

static u32
asm_b_or(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t or   \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);
		
	return lid;
}

static u32
asm_b_xor(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t xor  \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	asm_free_reg(ctx, rid);
		
	return lid;
}

static u32
asm_neg(env *ctx, u32 id) {
	fwprintf(ctx->out, L"\t neg  \t %s \n", ctx->r_name64[id]);
	return id;
}

static u32
asm_b_inv(env *ctx, u32 id) {
	fwprintf(ctx->out, L"\t not  \t %s \n", ctx->r_name64[id]);
	return id;
}

static u32
asm_b_lsh(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t mov  \t cl, %s \n", ctx->r_name8[rid]);
	fwprintf(ctx->out, L"\t shl  \t %s, cl \n", ctx->r_name64[lid]);
	asm_free_reg(ctx, rid);
		
	return lid;
}

static u32
asm_b_rsh(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t mov  \t cl, %s \n", ctx->r_name8[rid]);
	fwprintf(ctx->out, L"\t shr  \t %s, cl \n", ctx->r_name64[lid]);
	asm_free_reg(ctx, rid);
		
	return lid;
}

static u32
asm_l_not(env *ctx, u32 id) {
	fwprintf(ctx->out, L"\t test \t %s, %s \n", ctx->r_name64[id], ctx->r_name64[id]);
	fwprintf(ctx->out, L"\t sete \t %s \n", ctx->r_name8[id]);
	fwprintf(ctx->out, L"\t movzx\t %s, %s \n", ctx->r_name64[id], ctx->r_name8[id]);

	return id;
}

static u32
asm_l_and(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t test \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[lid]);
	fwprintf(ctx->out, L"\t setne\t %s \n", ctx->r_name8[lid]);
	fwprintf(ctx->out, L"\t test \t %s, %s \n", ctx->r_name64[rid], ctx->r_name64[rid]);
	fwprintf(ctx->out, L"\t setne\t %s \n", ctx->r_name8[rid]);
	fwprintf(ctx->out, L"\t and  \t %s, %s \n", ctx->r_name8[lid], ctx->r_name8[rid]);
	fwprintf(ctx->out, L"\t movzx\t %s, %s \n", ctx->r_name64[lid], ctx->r_name8[lid]);
	asm_free_reg(ctx, rid);

	return lid;
}

static u32
asm_l_or(env *ctx, u32 lid, u32 rid) {
	fwprintf(ctx->out, L"\t or   \t %s, %s \n", ctx->r_name64[lid], ctx->r_name64[rid]);
	fwprintf(ctx->out, L"\t setne\t %s \n", ctx->r_name8[lid]);
	fwprintf(ctx->out, L"\t movzx\t %s, %s\n", ctx->r_name64[lid], ctx->r_name8[lid]);
	asm_free_reg(ctx, rid);

	return lid;
}

static u32
asm_bool(env *ctx, u32 id, a_type type, u32 lbl) {
	fwprintf(ctx->out, L"\t test \t %s, %s \n", ctx->r_name64[id], ctx->r_name64[id]);
	
	if (type == A_IF    ||
	    type == A_WHILE ||
	    type == A_FOR) {
		fwprintf(ctx->out, L"\t je   \t L%u \n", lbl);
	} else {
		fwprintf(ctx->out, L"\t setnz\t %s \n", ctx->r_name8[id]);
		fwprintf(ctx->out, L"\t movsx\t %s, %s \n", ctx->r_name64[id], ctx->r_name8[id]);
	}

	return id;
}

static void
asm_func_pre(env *ctx, u32 id) {
#ifdef Z0_DEBUG
	log_info(L"գեներացվում է ֆունկցիան");
	log_info_table(ctx);
#endif
	ctx->f_lbl = asm_lbl_no();

	s32  cmp;
	cmp = wcscmp(ctx->g_name[id], L"գլխավոր");

	s32 *name;
	if (cmp == 0)
		name = wcsdup(L"main");
	else
		name = ctx->g_name[id];


	fwprintf(ctx->out, L"\t .text \n");
	fwprintf(ctx->out, L"\t .globl %ls \n", name);
	fwprintf(ctx->out, L"\t .type  %ls @function \n", name);
	fwprintf(ctx->out, L"%ls: \n", name);
	fwprintf(ctx->out, L"\t push \t rbp \n");
	fwprintf(ctx->out, L"\t mov  \t rbp, rsp \n");

	u32 preg;
	u32 poff;

	preg = 9;
	poff = 16; 

	u32 len;
	len = ctx->g_els[id] + id + 1;
	for (u32 i = id + 1; i < len; ++i) {
		if (preg >= 4) {
			ctx->g_ctype[i] = C_LOCAL;
			asm_locl_store(ctx, i, preg);
			--preg;
		} else {
			ctx->g_pos[i] = poff;
			poff += 8;//TODO: asm_type_size(ctx->g_ptype[i]);	
		}	
	}

#ifdef Z0_DEBUG
	log_info_u32(L"լոկալ հաշվիչ", ctx->l_off);
#endif

	ctx->s_off = (ctx->l_off + 15) & ~15;
	fwprintf(ctx->out, L"\t sub  \t rsp, %u \n", ctx->s_off);
}

static void
asm_func_post(env *ctx, u32 fid) {
	asm_lbl(ctx, ctx->f_lbl);
	if (ctx->g_ptype[fid] == P_VOID) 
		fwprintf(ctx->out, L"\t xor  \t eax, eax \n");

	fwprintf(ctx->out, L"\t add  \t rsp, %u \n", ctx->s_off);
	fwprintf(ctx->out, L"\t pop  \t rbp \n");
	fwprintf(ctx->out, L"\t ret \n");

#ifdef Z0_DEBUG
	log_info(L"գեներացվեց ֆունկցիան");
#endif
}

static u32
asm_func_call(env *ctx, node *tree) {
	node *args;
	args = tree->data[0];

	u32 id;
	for (u32 i = 0, r = 9; i < args->len; ++i, --r) {
		id = asm_ast_gen(ctx, args->data[i], 0xBAD, args->type);	

		if (r >= 4)
			fwprintf(ctx->out, L"\t mov  \t %s, %s \n", ctx->r_name64[r], ctx->r_name64[id]); 
		else 
			fwprintf(ctx->out, L"\t push \t %s \n", ctx->r_name64[id]); 

		asm_free_regs(ctx);		
	}

	fwprintf(ctx->out, L"\t call \t %ls \n", ctx->g_name[tree->id]); 

	if (args->len > 6)
		fwprintf(ctx->out, L"\t add \t rsp, %u \n", 8 * (args->len - 6)); 

	u32 reg;
	reg = asm_new_reg(ctx);
	fwprintf(ctx->out, L"\t mov  \t %s, rax \n", ctx->r_name64[reg]); 

	return reg;
}

static void
asm_func_ret(env *ctx, u32 id, u32 fid) {
	switch (ctx->g_ptype[fid]) {
	case P_U8:
	case P_S8:
		fwprintf(ctx->out, L"\t mov \t eax, %s \n", ctx->r_name8[id]);
		break;
	case P_U16:
	case P_S16:
		fwprintf(ctx->out, L"\t mov \t eax, %s \n", ctx->r_name16[id]);
		break;
	case P_U32:
	case P_S32:
		fwprintf(ctx->out, L"\t mov \t eax, %s \n", ctx->r_name32[id]);
		break;
	case P_U64:
	case P_S64:
		fwprintf(ctx->out, L"\t mov \t rax, %s \n", ctx->r_name64[id]);
		break;
	default:
		log_err(L"սխալ տիպի գեներացում ֆունկցիաի վերջնական մասը գեներացնելու ժամանակ");
	}

	asm_jmp(ctx, ctx->f_lbl);
}

static void
asm_ast_if(env *ctx, node *root) {
	u32 l_if;
	l_if = asm_lbl_no();

	u32 l_el;
	if (root->data[2] != NULL)
		l_el = asm_lbl_no();
	
	asm_ast_gen(ctx, root->data[0], l_if, root->type);
	asm_free_regs(ctx);

	asm_ast_gen(ctx, root->data[1], 0xBAD, root->type);
	asm_free_regs(ctx);

	if (root->data[2] != NULL)
		asm_jmp(ctx, l_el);
	asm_lbl(ctx, l_if);

	if (root->data[2] != NULL) {
		asm_ast_gen(ctx, root->data[2], l_el, root->type);
		asm_free_regs(ctx);
		asm_lbl(ctx, l_el);
	}
}

static void
asm_ast_while(env *ctx, node *root) {
	u32 l_start;
	l_start = asm_lbl_no();
	asm_lbl(ctx, l_start);

	u32 l_end;
	l_end = asm_lbl_no();

	asm_ast_gen(ctx, root->data[0], l_end, root->type);	
	asm_free_regs(ctx);

	asm_ast_gen(ctx, root->data[1], 0xBAD, root->type);
	asm_free_regs(ctx);

	asm_jmp(ctx, l_start);
	asm_lbl(ctx, l_end);
}

static void
asm_ast_for(env *ctx, node *root) {
	asm_ast_gen(ctx, root->data[0], 0xBAD, root->type);

	u32 l_start;
	l_start = asm_lbl_no();
	asm_lbl(ctx, l_start);

	u32 l_end;
	l_end = asm_lbl_no();
	asm_ast_gen(ctx, root->data[1], l_end, root->type);
	asm_free_regs(ctx);

	asm_ast_gen(ctx, root->data[3], 0xBAD, root->type);
	asm_free_regs(ctx);

	asm_ast_gen(ctx, root->data[2], 0xBAD, root->type);
	asm_free_regs(ctx);
		
	asm_jmp(ctx, l_start);
	asm_lbl(ctx, l_end);
}

static u32
asm_ast_gen(env *ctx, node *root, u32 lbl, a_type ptype) {
	switch (root->type) {
	case A_IF:
		asm_ast_if(ctx, root);
		return 0xBAD;
	case A_WHILE:
		asm_ast_while(ctx, root);
		return 0xBAD;
	case A_FOR:
		asm_ast_for(ctx, root);
		return 0xBAD;
	case A_FUNC:
		ctx->l_off = 0;
		if (root->len > 1) {
			asm_ast_gen(ctx, root->data[0], 0xBAD, root->type);
			asm_func_pre(ctx, root->id);
			asm_ast_gen(ctx, root->data[1], 0xBAD, root->type);
		} else {
			asm_func_pre(ctx, root->id);
			asm_ast_gen(ctx, root->data[0], 0xBAD, root->type);
		}

		asm_func_post(ctx, root->id);
		return 0xBAD;
	case A_GROUP:
		if (ptype == A_CALL)
			return 0xBAD;

		for (u32 i = 0; i < root->len; ++i) {
			asm_ast_gen(ctx, root->data[i], 0xBAD, root->type);
			asm_free_regs(ctx);
		}
	
		return 0xBAD;
	default:
		break;
	}

	u32 lid;
	lid = 0;
	if (root->len > 0)
		lid = asm_ast_gen(ctx, root->data[0], 0xBAD, root->type);

	u32 rid;
	rid = 0;
	if (root->len > 1)
		rid = asm_ast_gen(ctx, root->data[1], 0xBAD, root->type);

	switch (root->type) {
	case A_B_AND:
		return asm_b_and(ctx, lid, rid);
	case A_B_OR:
		return asm_b_or(ctx, lid, rid);
	case A_B_XOR:
		return asm_b_xor(ctx, lid, rid);
	case A_NEG:
		return asm_neg(ctx, lid);
	case A_B_INV:
		return asm_b_inv(ctx, lid);
	case A_B_LSH:
		return asm_b_lsh(ctx, lid, rid);
	case A_B_RSH:
		return asm_b_rsh(ctx, lid, rid);
	case A_ADD:
		return asm_add(ctx, lid, rid);
	case A_SUB:
		return asm_sub(ctx, lid, rid);
	case A_MUL:
		return asm_mul(ctx, lid, rid);
	case A_DIV:
		return asm_div(ctx, lid, rid);
	case A_L_NOT:
		return asm_l_not(ctx, lid);
	case A_L_AND:
		return asm_l_and(ctx, lid, rid);
	case A_L_OR:
		return asm_l_or(ctx, lid, rid);
	case A_BOOL:
		return asm_bool(ctx, lid, ptype, lbl);
	case A_PRE_INC:
	case A_PRE_DEC:
		return asm_glob_load(ctx, root->data[0]->id, root->type);
	case A_POST_INC:
	case A_POST_DEC:
		return asm_glob_load(ctx, root->id, root->type);
	case A_EQ:
	case A_NE:
	case A_LT:
	case A_GT:
	case A_LE:
	case A_GE:
		return asm_cmp_set(ctx, lid, rid, root->type);
	case A_INT:
		return asm_mov_int(ctx, root->val);
	case A_STR:
		return asm_load_str(ctx, root->lbl);
	case A_ADDR:
		return asm_addr(ctx, root->id);
	case A_ASSIGN:
		switch (root->data[1]->type) {
		case A_DREF:
			return asm_ptr_store(ctx, rid, lid, root->data[1]->prim);
		case A_IDENT:
			if (ctx->g_ctype[root->data[1]->id] == C_GLOBAL)	
				return asm_glob_store(ctx, root->data[1]->id, lid);
			else
				return asm_locl_store(ctx, root->data[1]->id, lid);
		default:
			log_err(L"սխալ աջ կողմի տիպ, վերագրումը գեներացնելու ժամանակ");
			break;
		}
		break;
	case A_DREF:
		if (root->lval)
			return lid;
		else
			return asm_dref(ctx, lid, root->data[0]->prim);
	case A_IDENT:
		if (!root->lval || ptype == A_DREF) {
			if (ctx->g_ctype[root->id] == C_GLOBAL)	
				return asm_glob_load(ctx, root->id, root->type);
			else
				return asm_locl_load(ctx, root->id, root->type);
		} else {
			return lid;
		}
	case A_VAR:
		switch (ctx->g_ctype[root->id]) {
		case C_LOCAL:
		case C_PARAM:
		{
			u32 size;
			size = asm_type_size(ctx->g_ptype[root->id]);

			u32 off;
			off = size < 4 ? 4 : size; 

#ifdef Z0_DEBUG
			log_info_u32(L"լոկալ հաշվիչին ավելացվեց", off);
#endif

			ctx->l_off += off;
			ctx->g_pos[root->id] = ctx->l_off;
			break; 
		}
		case C_GLOBAL:
			asm_glob(ctx, root->id);
			break;
		default:	
			log_err(L"չճանածված հիշողության տեսակ");
		}

		return 0xBAD;
	case A_SCALE:
		rid = asm_mov_int(ctx, root->size);
		return asm_mul(ctx, lid, rid);
	case A_CALL:
		return asm_func_call(ctx, root);
	case A_RET:
		asm_func_ret(ctx, lid, root->id);
		return 0xBAD;
	default:
		log_err(L"չճանաչված «AST» օպերատոր");
		return 0xBAD;
	}

	return 0xBAD;
} 

void
asm_gen(env *ctx) {
#ifdef Z0_DEBUG
	log_info(L"սկսվում է գեներացվել ՄԿ֊ը");
#endif

	fwprintf(ctx->out, L".intel_syntax noprefix \n");
	fwprintf(ctx->out, L"\t .text \n");
	fwprintf(ctx->out, L".LC0: \n");
	fwprintf(ctx->out, L"\t .string \"%%d\\n\" \n");
	fwprintf(ctx->out, L"տպիր: \n");
	fwprintf(ctx->out, L"\t push \t rbp \n");
	fwprintf(ctx->out, L"\t mov  \t rbp, rsp \n");
	fwprintf(ctx->out, L"\t sub  \t rsp, 16 \n");
	fwprintf(ctx->out, L"\t mov  \t [rbp - 4], edi \n");
	fwprintf(ctx->out, L"\t mov  \t eax, [rbp - 4] \n");
	fwprintf(ctx->out, L"\t mov  \t esi, eax \n");
	fwprintf(ctx->out, L"\t mov  \t rdi, OFFSET FLAT:.LC0 \n");
	fwprintf(ctx->out, L"\t xor  \t eax, eax \n");
	fwprintf(ctx->out, L"\t call \t printf@PLT \n");
	fwprintf(ctx->out, L"\t nop \n");
	fwprintf(ctx->out, L"\t leave \n");
	fwprintf(ctx->out, L"\t ret \n");

	asm_ast_gen(ctx, ctx->root, 0xBAD, 0xBAD);

#ifdef Z0_DEBUG
	log_info(L"ավարտվեց ՄԿ֊ի գեներացումը");
#endif
}

void
asm_free(env *ctx) {
	free(ctx->g_pos);
	free(ctx->g_ctype);
	free(ctx->g_els);
	free(ctx->g_stype);
	free(ctx->g_ptype);
	for (u32 i = 0; i < ctx->g_cap; ++i)
		free(ctx->g_name[i]);
	free(ctx->g_name);

	for (u32 i = 0; i < 10; ++i)
		free(ctx->r_name8[i]);
	for (u32 i = 0; i < 10; ++i)
		free(ctx->r_name16[i]);
	for (u32 i = 0; i < 10; ++i)
		free(ctx->r_name32[i]);
	for (u32 i = 0; i < 10; ++i)
		free(ctx->r_name64[i]);

	fclose(ctx->out);

#ifdef Z0_DEBUG
	log_info(L"ոչնչացվեց «asm»֊ն");
#endif
}
