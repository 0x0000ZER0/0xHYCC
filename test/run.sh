#!/bin/sh

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;36m'
DEF='\033[0m'

make

n=1

for i in ./test/փորձ_*
do
	echo -n "./test/փորձ_$n.հյ"
	echo -n "......................"

	if [ ! -f "./test/արդյունք_$n" ]
	then
		echo "	${BLUE}ԱՆՀԱՅՏ${DEF}:"
	else
		./bin/0xHYCC "./test/փորձ_$n.հյ"
		gcc -o ./bin/a.out ./bin/hycc.s -no-pie

		./bin/a.out > ./test/temp

		if cmp -s "./test/temp" "./test/արդյունք_$n"
		then
			echo "	${GREEN}ՃԻՇՏ${DEF}:"
		else
			echo "	${RED}ՍԽԱԼ${DEF}:"
		fi


		rm ./test/temp
	fi

	n=$((n + 1))
done
