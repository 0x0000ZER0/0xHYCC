#ifndef DEFS_H
#define DEFS_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

// ======= TYPE =======
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef enum {
	T_L_NOT,	
	T_B_INV,
	T_INC,
	T_DEC,
	T_STAR,
	T_SLASH,
	T_PLUS,
	T_MINUS,
	T_B_LSH,
	T_B_RSH,
	T_LT,
	T_GT,
	T_LE,
	T_GE,
	T_EQ,
	T_NE,
	T_AMPER,
	T_B_XOR,
	T_B_OR,
	T_L_AND,
	T_L_OR,
	T_ASSIGN,

	T_EOF,

	T_INT,
	T_STR,
	T_SEMI,
	T_COMMA,
	T_VOID,
	T_U8,
	T_U16,
	T_U32,
	T_U64,
	T_S8,
	T_S16,
	T_S32,
	T_S64,
	T_IDENT,
	T_LPAREN,
	T_RPAREN,
	T_LBRACE,
	T_RBRACE,
	T_LSQUARE,
	T_RSQUARE,
	T_IF,
	T_ELSE,
	T_WHILE,
	T_FOR,
	T_RET,

	T_NONE
} t_type;

typedef struct {
	t_type type;
	s32    val;
} token;

typedef enum {
	A_L_NOT,	
	A_B_INV, 
	A_PRE_INC,
	A_PRE_DEC,

	A_MUL	 = T_STAR,
	A_DIV	 = T_SLASH,
	A_ADD	 = T_PLUS,
	A_SUB	 = T_MINUS,
	A_B_LSH  = T_B_LSH,
	A_B_RSH  = T_B_RSH,
	A_LT 	 = T_LT,
	A_GT 	 = T_GT,
	A_LE 	 = T_LE,
	A_GE 	 = T_GE,
	A_EQ 	 = T_EQ,
	A_NE 	 = T_NE,
	A_B_AND  = T_AMPER,
	A_B_XOR  = T_B_XOR,
	A_B_OR   = T_B_OR,
	A_L_AND  = T_L_AND,
	A_L_OR   = T_L_OR,
	A_ASSIGN = T_ASSIGN,

	A_POST_INC,
	A_POST_DEC,
	A_IDENT,
	A_INT,
	A_STR,
	A_VAR,
	A_IF,
	A_WHILE,
	A_FOR,
	A_FUNC,
	A_CALL,
	A_RET,
	A_ADDR,
	A_DREF,
	A_SCALE,
	A_NEG,
	A_BOOL,
	A_GROUP
} a_type;

typedef enum {
	P_NONE = 0x0,
	P_VOID,
	P_U8,
	P_U16,
	P_U32, 
	P_U64,
	P_S8,
	P_S16,
	P_S32, 
	P_S64,
	P_VOID_PTR,
	P_U8_PTR,
	P_U16_PTR,
	P_U32_PTR,
	P_U64_PTR,
	P_S8_PTR,
	P_S16_PTR,
	P_S32_PTR,
	P_S64_PTR
} p_type;

typedef struct node node;
struct node {
	a_type   type;
	p_type	 prim;
	bool	 lval;
	u32	 cap;
	u32	 len;
	node   **data;
	union {
		s32 val;
		u32 id;
		u32 lbl;
		u32 size;
	};
};

typedef enum {
	S_VAR,
	S_FUNC,
	S_ARR
} s_type;

typedef enum {
	C_LOCAL  = 0x1,
	C_PARAM  = 0x2,
	C_GLOBAL = 0x4
} c_type;

typedef struct {
	token    tok;
	token    rej;

	u32	 f_id;
	u32	 f_lbl;

	u32      w_max;
	s32     *word;
	s32      put;
	u32      line;

	FILE    *in;
	FILE    *out;

	node    *root;

	u32    	 g_cap;
	u32    	 g_len;
	s32    **g_name;
	p_type  *g_ptype;
	s_type  *g_stype;
	u32	*g_els;
	c_type  *g_ctype;
	u32	*g_pos;

	u32	 l_off;
	u32	 s_off;

	bool     r_free[4];
	u8      *r_name64[10];
	u8      *r_name32[10];
	u8      *r_name16[10];
	u8      *r_name8[10];
} env;

// ======= FUNC =======
void
log_err(s32*);

void
log_warn(s32*);

void
log_info(s32*);

void
log_info_u32(s32*, u32);

void
log_info_tok(s32*, token);

void
log_info_tree(node*);

void
log_info_prim(s32*, p_type);

void
log_info_table(env*);

bool
lex_scan(env*);

void
ast_init(env*, char*);

void
ast_read(env*);

void
ast_free(env*);

void
asm_init(env*);

u32
asm_glob_str(env*);

u32
asm_item_add(env*, p_type, s_type, u32, c_type);

u32
asm_item_find(env*, c_type);

u32
asm_type_size(p_type);

void
asm_gen(env*);

void
asm_free(env*);

#endif
