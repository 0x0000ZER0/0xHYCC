#include "defs.h"

#include <wchar.h>

static s32
next(env *ctx) {
	s32 sym;
	
	if (ctx->put != 0) {
		sym = ctx->put;
		ctx->put = 0;

		return sym;
	}

	sym = fgetwc(ctx->in);
	if (sym == L'\n')
		++ctx->line;

	return sym;
}

static s32
scan_s32(env *ctx, s32 sym) {
	s32 val;
	val = sym - L'0';

	sym = next(ctx);
	while (sym >= L'0' && sym <= L'9') {
		val *= 10;
		val += sym - L'0';

		sym = next(ctx); 
	}

	ctx->put = sym;

	return val;
}

static void
scan_word(env *ctx, s32 sym) {
	u32 idx;
	idx = 0;	

	ctx->word[idx] = sym;

	sym = next(ctx);

	while ((sym >= L'ա' && sym <= L'ֆ') ||
	       (sym >= L'Ա' && sym <= L'Ֆ') ||
	       (sym >= L'0' && sym <= L'9') ||
		sym == L'_') {
		if (idx >= 510)
			log_err(L"բառի տառերի առավելագույն քանակը {512} է");
	
		++idx;
		ctx->word[idx] = sym; 

		sym = next(ctx);
	}

	ctx->put = sym;
	ctx->word[idx + 1] = L'\0';
}

bool
lex_scan(env *ctx) {
	if (ctx->rej.type != T_NONE) {
		ctx->tok = ctx->rej;
		ctx->rej.type = T_NONE;
		return true;
	}

	s32 sym;
	sym = next(ctx);

	while (sym == L' ' || sym == L'\n' || sym == L'\t')
		sym = next(ctx);

	switch (sym) {
	case WEOF:
		ctx->tok.type = T_EOF;
		return false;
	case L'+':
		sym = next(ctx);
		
		if (sym == L'+') {
			ctx->tok.type = T_INC;
		} else {
			ctx->tok.type = T_PLUS;
			ctx->put = sym;
		}
		break;
	case L'-':
		sym = next(ctx);
		
		if (sym == L'-') {
			ctx->tok.type = T_DEC;
		} else {
			ctx->tok.type = T_MINUS;
			ctx->put = sym;
		}
		break;
	case L'*':
		ctx->tok.type = T_STAR;
		break;
	case L'/':
		sym = next(ctx);
	
		if (sym == L'/') {
			while (sym != L'\n')
				sym = next(ctx);
			
			return lex_scan(ctx);
		} else if (sym == L'*') {
			while (true) {
				sym = next(ctx);
				if (sym == L'*') {
					sym = next(ctx);
					if (sym == L'/')
						break;
				}
			}

			return lex_scan(ctx);
		} 

		ctx->tok.type = T_SLASH;		
		break;
	case L';':
		ctx->tok.type = T_SEMI;
		break;
	case L',':
		ctx->tok.type = T_COMMA;
		break;
	case L'\'':
		sym = next(ctx);

		ctx->tok.type = T_INT;
		ctx->tok.val  = sym;

		sym = next(ctx);
		if (sym != L'\'')
			log_err(L"«'»֊ով սկսվող տառը պետք է վերջանա «'»֊ով");
		break;
	case L'\"':
	{
		u32 idx;
		idx = 0;

		do {
			++idx;
			if (idx >= 510)
				log_err(L"բառի տառերի առավելագույն քանակը {512} է");

			sym = next(ctx);
			ctx->word[idx] = sym;
		} while (sym != L'\"');
		ctx->word[idx] = L'\0';
		ctx->tok.type  = T_STR;
		break;
	}
	case L'(':
		ctx->tok.type = T_LPAREN;
		break;
	case L')':
		ctx->tok.type = T_RPAREN;
		break;
	case L'[':
		ctx->tok.type = T_LSQUARE;
		break;
	case L']':
		ctx->tok.type = T_RSQUARE;
		break;
	case L'{':
		ctx->tok.type = T_LBRACE;
		break;
	case L'}':
		ctx->tok.type = T_RBRACE;
		break;
	case L'&':
		ctx->tok.type = T_AMPER;
		break;
	case L'|':
		ctx->tok.type = T_B_OR;
		break;
	case L'^':
		ctx->tok.type = T_B_XOR;
		break;
	case L'~':
		ctx->tok.type = T_B_INV;
		break;
	case L'=':
		sym = next(ctx);

		if (sym == L'=') {
			ctx->tok.type = T_EQ;
		} else {
			ctx->tok.type = T_ASSIGN;		
			ctx->put = sym;
		}
	
		break;
	case L'!':
		sym = next(ctx);
		
		if (sym == L'=') {
			ctx->tok.type = T_NE;
		} else {
			ctx->tok.type = T_L_NOT;
			ctx->put = sym;
		}

		break;
	case L'<':
		sym = next(ctx);
	
		if (sym == L'=') {
			ctx->tok.type = T_LE;
		} else if (sym == L'<') {
			ctx->tok.type = T_B_LSH;
		} else {
			ctx->tok.type = T_LT;
			ctx->put = sym;
		}

		break;
	case L'>':
		sym = next(ctx);

		if (sym == L'=') {
			ctx->tok.type = T_GE;
		} else if (sym == L'>') {
			ctx->tok.type = T_B_RSH;
		} else {
			ctx->tok.type = T_GT;
			ctx->put = sym;
		}
	
		break;
	default:
		if (sym >= L'0' && sym <= L'9') {
			ctx->tok.type = T_INT;
			ctx->tok.val  = scan_s32(ctx, sym);
			break;
		} else if ((sym >= L'ա' && sym <= L'և') ||
			   (sym >= L'Ա' && sym <= L'Ֆ') ||
			    sym == L'_') {
			scan_word(ctx, sym);			

			s32 cmp;
			switch (ctx->word[0]) {
			case L'տ':
				cmp = wcscmp(ctx->word, L"տուր");
				if (cmp == 0) { 
					ctx->tok.type = T_RET;
					break;
				}

				ctx->tok.type = T_IDENT;
				break;
			case L'ն':
				cmp = wcscmp(ctx->word, L"ն8");
				if (cmp == 0) {
					ctx->tok.type = T_S8;
					break;
				}

				cmp = wcscmp(ctx->word, L"ն16");
				if (cmp == 0) {
					ctx->tok.type = T_S16;
					break;
				}

				cmp = wcscmp(ctx->word, L"ն32");
				if (cmp == 0) {
					ctx->tok.type = T_S32;
					break;
				}
				
				cmp = wcscmp(ctx->word, L"ն64");
				if (cmp == 0) {
					ctx->tok.type = T_S64;
					break;
				}

				ctx->tok.type = T_IDENT;
				break;
			case L'ե':
				cmp = wcscmp(ctx->word, L"եթե");
	
				if (cmp == 0)
					ctx->tok.type = T_IF;
				else
					ctx->tok.type = T_IDENT;

				break;
			case L'ա':
				cmp = wcscmp(ctx->word, L"ա8");
				if (cmp == 0) {
					ctx->tok.type = T_U8;
					break;
				}

				cmp = wcscmp(ctx->word, L"ա16");
				if (cmp == 0) {
					ctx->tok.type = T_U16;
					break;
				}

				cmp = wcscmp(ctx->word, L"ա32");
				if (cmp == 0) {
					ctx->tok.type = T_U32;
					break;
				}
				
				cmp = wcscmp(ctx->word, L"ա64");
				if (cmp == 0) {
					ctx->tok.type = T_U64;
					break;
				}

				cmp = wcscmp(ctx->word, L"այլ");
				if (cmp == 0) {
					ctx->tok.type = T_ELSE;
					break;
				}
					
				ctx->tok.type = T_IDENT;
				break;
			case L'մ':
				cmp = wcscmp(ctx->word, L"մինչդեռ");

				if (cmp == 0)
					ctx->tok.type = T_WHILE;
				else
					ctx->tok.type = T_IDENT;
	
				break;
			case L'դ':
				cmp = wcscmp(ctx->word, L"դեպի");
				if (cmp == 0) {
					ctx->tok.type = T_FOR;
					break;
				}

				cmp = wcscmp(ctx->word, L"դատարկ");
				if (cmp == 0) {
					ctx->tok.type = T_VOID;
					break;
				}

				ctx->tok.type = T_IDENT;
				break;
			case L'և':
				ctx->tok.type = T_L_AND;
				break;
			case L'կ':
				cmp = wcscmp(ctx->word, L"կամ");
				if (cmp == 0) {
					ctx->tok.type = T_L_OR;
					break;
				}

				ctx->tok.type = T_IDENT;
				break;
			default:
				ctx->tok.type = T_IDENT;
				break;
			}

			break;
		}

		log_err(L"չճանաչված նշան");
	}

#ifdef Z0_DEBUG
	log_info_tok(L"ավարտվեց «նշանի» ճանաչման սկանը", ctx->tok);
#endif
	return true;
}
