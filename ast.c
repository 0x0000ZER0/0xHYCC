#include "defs.h"

#include <malloc.h>
#include <locale.h>
#include <wchar.h>

// ================ DECL ================

static node*
ast_stmt_block(env*);

static node*
ast_stmt_single(env*);

static node*
ast_expr(env*, u32);

// ================ IMPL ================

static node*
ast_node_init(a_type  type, p_type prim, s32 val) {
	node *parent;
	parent = malloc(sizeof (node));
	if (parent == NULL)
		log_err(L"չստացվեց տարածք հատկացնել «AST» մասնիկի համար");

	parent->type = type;
	parent->prim = prim;
	parent->lval = false;
	parent->val  = val;
	parent->cap  = 4;
	parent->len  = 0;
	parent->data = malloc(parent->cap * sizeof (node*));

	return parent;
}

static void
ast_node_add(node *parent, node *child) {
	if (parent->cap == parent->len) {
		parent->cap *= 2;
		parent->data = realloc(parent->data, parent->cap * sizeof (node*));
		if (parent == NULL)
			log_err(L"չստացվեց կրկին տարածք հատկացնել «AST» մասնիկի համար");
	}

	parent->data[parent->len] = child;
	++parent->len;
}

static p_type
ast_to_ptr(p_type prim) {
	if (prim < P_VOID || prim > P_S64)
		log_err(L"չճանաչված հասարակ տիպ");

	return prim + P_S64;	
}

static p_type
ast_val_at(p_type prim) {
	if (prim < P_VOID_PTR || prim > P_S64_PTR)
		log_err(L"չճանաչված հասարակ տիպ");

	return prim - P_S64;	
}

static node*
ast_type_check(node *tree, p_type rprim, a_type type) {
	p_type lprim;
	lprim = tree->prim;

#ifdef Z0_DEBUG
	log_info_prim(L"ստուգվում է աջ  կողմի տիպը", rprim);	
	log_info_prim(L"ստուգվում է ձախ կողմի տիպը", lprim);	
#endif

	if ((lprim >= P_U8 && lprim <= P_S64) &&
	    (rprim >= P_U8 && rprim <= P_S64)) {
		u32 lsize;
		lsize = asm_type_size(lprim);

		u32 rsize;
		rsize = asm_type_size(rprim);

		if (rsize >= lsize)
			return tree;

		if (lsize > rsize)
			return NULL;
	}

	if ((lprim >= P_U8_PTR && lprim <= P_S64_PTR) &&
	    (type == 0xBAD && rprim == lprim))
		return tree;

	if (type == A_ADD || type == A_SUB) {
		if ((lprim >= P_U8     && lprim <= P_S64) &&
		    (rprim >= P_U8_PTR && rprim <= P_S64_PTR)) {
			p_type actual;
			actual = ast_val_at(rprim);

			u32 rsize;
			rsize = asm_type_size(actual);

			node *scale;
			scale = ast_node_init(A_SCALE, rprim, rsize);
			ast_node_add(scale, tree);

			return scale;
		}
	}

	return NULL;
}

static node*
ast_prefix(env *ctx) {
	node *next;
	next = NULL;

	switch (ctx->tok.type) {
	case T_AMPER:
		lex_scan(ctx);
		next = ast_prefix(ctx);

		if (next->type != A_IDENT)
			log_err(L"«&» պետք է հաջորդի փոփոխականի անունը");
		
		next->type = A_ADDR;
		next->prim = ast_to_ptr(next->prim);
		break;
	case T_STAR:
	{
		lex_scan(ctx);

		node *tree;
		tree = ast_prefix(ctx);

		if (tree->type != A_IDENT && tree->type != A_DREF)
			log_err(L"«*»-ից հետո պետք է լինի մեկ այլ «*» կամ փոփոխականի անուն");

		p_type prim;
		prim = ast_val_at(tree->prim);

		next = ast_node_init(A_DREF, prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_MINUS:
	{
		lex_scan(ctx);

		node *tree;
		tree = ast_prefix(ctx);

		next = ast_node_init(A_NEG, tree->prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_B_INV:
	{
		lex_scan(ctx);

		node *tree;
		tree = ast_prefix(ctx);

		next = ast_node_init(A_B_INV, tree->prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_L_NOT:
	{
		lex_scan(ctx);
		
		node *tree;
		tree = ast_prefix(ctx);

		next = ast_node_init(A_L_NOT, tree->prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_INC:
	{
		lex_scan(ctx);

		if (ctx->tok.type != T_IDENT)
			log_err(L"«++»֊ին պետք է հաջորդի փոփոխականի անունը");

		node *tree;
		tree = ast_prefix(ctx);

		next = ast_node_init(A_PRE_INC, tree->prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_DEC:
	{
		lex_scan(ctx);

		if (ctx->tok.type != T_IDENT)
			log_err(L"«--»֊ին պետք է հաջորդի փոփոխականի անունը");

		node *tree;
		tree = ast_prefix(ctx);

		next = ast_node_init(A_PRE_DEC, tree->prim, 0xBAD);
		ast_node_add(next, tree);
		break;
	}
	case T_STR:
	{
		u32 lbl;
		lbl = asm_glob_str(ctx);	

		next = ast_node_init(A_STR, P_S32_PTR, lbl);
		lex_scan(ctx);
		break;
	}
	case T_INT:
		if (ctx->tok.val >= INT8_MIN && ctx->tok.val <= INT8_MAX)
			next = ast_node_init(A_INT, P_S8, ctx->tok.val);
		else if (ctx->tok.val >= INT16_MIN && ctx->tok.val <= INT16_MAX)
			next = ast_node_init(A_INT, P_S16, ctx->tok.val);
		else if (ctx->tok.val >= INT32_MIN && ctx->tok.val <= INT32_MAX)
			next = ast_node_init(A_INT, P_S32, ctx->tok.val);
		else if (ctx->tok.val >= INT64_MIN && ctx->tok.val <= INT64_MAX)
			next = ast_node_init(A_INT, P_S64, ctx->tok.val);
		else if (ctx->tok.val >= 0 && ctx->tok.val <= UINT8_MAX)
			next = ast_node_init(A_INT, P_U8, ctx->tok.val);
		else if (ctx->tok.val >= 0 && ctx->tok.val <= UINT16_MAX)
			next = ast_node_init(A_INT, P_U16, ctx->tok.val);
		else if (ctx->tok.val >= 0 && ctx->tok.val <= UINT32_MAX)
			next = ast_node_init(A_INT, P_U32, ctx->tok.val);
		else if (ctx->tok.val >= 0 && ctx->tok.val <= UINT64_MAX)
			next = ast_node_init(A_INT, P_U64, ctx->tok.val);

		lex_scan(ctx);
		break;
	case T_IDENT:
	{
		u32 id;
		id = asm_item_find(ctx, C_LOCAL | C_GLOBAL | C_PARAM);	
		if (id == ctx->g_cap)
			log_err(L"չգտնվեց տվյալ անունը");

		lex_scan(ctx);
		
		if (ctx->tok.type == T_LPAREN) {
			if (ctx->g_stype[id] != S_FUNC)
				log_err(L"ֆունկցիան սահմանված չէ");

			lex_scan(ctx);

			node *args;
			args = ast_node_init(A_GROUP, P_NONE, 0xBAD);

			node *arg;
			while (ctx->tok.type != T_RPAREN) {
				arg = ast_expr(ctx, 0);
				ast_node_add(args, arg);

				if (ctx->tok.type == T_COMMA) {
					lex_scan(ctx);
					continue;
				}

				if (ctx->tok.type == T_RPAREN)
					break;
			
				log_err(L"ֆունկցիաի արգումենտնեռը տալու վերջում պետք է դնել «)» նշանը");
			}

			node *tree;
			tree = ast_node_init(A_CALL, ctx->g_ptype[id], id);
			ast_node_add(tree, args);
			
			if (ctx->tok.type != T_RPAREN)
				log_err(L"ֆունկցիան կանչելուց պետք է վերջում դնել «)»-ը");

			next = tree;
		} else if (ctx->tok.type == T_LSQUARE) {
			if (ctx->g_stype[id] != S_ARR)
				log_err(L"զանգվածը սահմանված չէ");

			node *addr;
			addr = ast_node_init(A_ADDR, ctx->g_ptype[id], id);	

			lex_scan(ctx);

			node *index;
			index = ast_expr(ctx, 0);
			
			if (index->prim < P_U8 || index->prim > P_S64)
				log_err(L"զանգվածի էլեմենտի ինդեքսը գրելիս պետք է օգտագործել թվեր");

			if (ctx->tok.type != T_RSQUARE)
				log_err(L"զանգվածի ինդեքսից հետո պետք է դնել «]»");

			index = ast_type_check(index, ctx->g_ptype[id], A_ADD); 

			node *off;
			off = ast_node_init(A_ADD, ctx->g_ptype[id], 0xBAD);
			ast_node_add(off, addr);
			ast_node_add(off, index);

			p_type actual;
			actual = ast_val_at(ctx->g_ptype[id]);

			next = ast_node_init(A_DREF, actual, 0xBAD);
			ast_node_add(next, off);
		} else {
			ctx->rej = ctx->tok;

			if (ctx->g_stype[id] != S_VAR)
				log_err(L"փոփոխականը սահմանված չէ");

			switch (ctx->tok.type) {
			case T_INC:
				lex_scan(ctx);

				next = ast_node_init(A_POST_INC, ctx->g_ptype[id], id);
				break;
			case T_DEC:
				lex_scan(ctx);
				
				next = ast_node_init(A_POST_DEC, ctx->g_ptype[id], id);
				break;
			default:
				next = ast_node_init(A_IDENT, ctx->g_ptype[id], id);
				break;
			}
		}

		lex_scan(ctx);
		break;
	}
	case T_LPAREN:
		lex_scan(ctx);
		next = ast_expr(ctx, 0);

		if (ctx->tok.type != T_RPAREN)
			log_err(L"պակասում է «)»-ը");
		lex_scan(ctx);
		break;
	default:
		log_err(L"օպերատորների միջև պետք է լինեն թվեր կամ փոփոխականի անուններ");
	}

	return next;
}

static node*
ast_expr(env *ctx, u32 prec) {
	static u32 P[] = {
		13, // T_L_NOT	
		13, // T_B_INV
		13, // T_INC	
		13, // T_DEC	
		12, // T_STAR
		12, // T_SLASH
		11, // T_PLUS
		11, // T_MINUS
		10, // T_B_LSH,
		10, // T_B_RSH,
		9,  // T_LT
		9,  // T_GT
		9,  // T_LE
		9,  // T_GE
		8,  // T_EQ
		8,  // T_NE
		7,  // T_AMPER	
		6,  // T_B_XOR
		5,  // T_B_OR
		4,  // T_L_AND
		3,  // T_L_OR
		2,  // T_ASSIGN
		1,  // T_EOF
	};

	node *left;
	left = ast_prefix(ctx);
	
	t_type type;
	type = ctx->tok.type;

	if (type == T_SEMI || type == T_RPAREN || type == T_RSQUARE)
		return left;

	node *right;
	node *temp;
	while ((P[type] > prec) ||
	       (type == T_ASSIGN && P[type] == prec)) {
		lex_scan(ctx);

		right = ast_expr(ctx, P[type]);

		if (type != T_ASSIGN) {
			node *ltemp;
			ltemp = ast_type_check(left, right->prim, (a_type)type);
			node *rtemp;
			rtemp = ast_type_check(right, left->prim, (a_type)type);

			if (ltemp == NULL && rtemp == NULL)
				log_err(L"անհամատեղելի տիպեր գործողության ժամանակ");

			if (ltemp != NULL)
				left = ltemp;

			if (rtemp != NULL)
				right = rtemp;
		} else {
			left->lval = true;

			right = ast_type_check(right, left->prim, 0xBAD);
			if (right == NULL)
				log_err(L"անհամատեղելի տիպեր վերագրման գործողության ժամանակ");

			temp = left;

			left  = right;
			right = temp;	
		}	

		temp = left;

		left = ast_node_init((a_type)type, left->prim, 0xBAD);
		ast_node_add(left, temp);
		ast_node_add(left, right);
	
		type = ctx->tok.type;	
		if (type == T_SEMI || type == T_RPAREN || type == T_RSQUARE)
			return left;
	}

	return left;
}

static p_type
ast_prim_type(env *ctx) {
	if (ctx->tok.type < T_VOID || ctx->tok.type > T_S64)
		log_err(L"չճանաչված նշանի տիպ");

	p_type prim;
	prim = ctx->tok.type - T_VOID + 1;

	do {
		lex_scan(ctx);
		if (ctx->tok.type != T_STAR)
			break;
		prim = ast_to_ptr(prim);
	} while (true);

	return prim;
}

static node*
ast_stmt_if(env *ctx) {
	lex_scan(ctx);

	if (ctx->tok.type != T_LPAREN)
		log_err(L"«եթե»֊ից հետո պետք է լինի «(»");
	lex_scan(ctx);

	node *cond;
	cond = ast_expr(ctx, 0);

	node *left;
	left = ast_node_init(A_BOOL, cond->prim, 0xBAD);
	ast_node_add(left, cond);

	if (ctx->tok.type != T_RPAREN)
		log_err(L"պակասում է «)»-ը");
	lex_scan(ctx);

	node *mid;
	mid = ast_stmt_block(ctx);

	node *right;
	right = NULL;

	if (ctx->tok.type == T_ELSE) {
		lex_scan(ctx);
		right = ast_stmt_block(ctx);
	}

	node *tree;
	tree = ast_node_init(A_IF, P_NONE, 0xBAD);
	ast_node_add(tree, left);
	ast_node_add(tree, mid);
	ast_node_add(tree, right);

	return tree;
}

static node*
ast_stmt_while(env *ctx) {
	lex_scan(ctx);
	
	if (ctx->tok.type != T_LPAREN)
		log_err(L"«մինչդեռ»֊ից հետո պետք է լինի «(»");
	lex_scan(ctx);

	node *cond;
	cond = ast_expr(ctx, 0);

	node *left;
	left = ast_node_init(A_BOOL, cond->prim, 0xBAD);
	ast_node_add(left, cond);

	if (ctx->tok.type != T_RPAREN)	
		log_err(L"պակասում է «)»-ը");
	lex_scan(ctx);
	
	node *mid;
	mid = ast_stmt_block(ctx);

	node *tree;
	tree = ast_node_init(A_WHILE, P_NONE, 0xBAD);
	ast_node_add(tree, left);
	ast_node_add(tree, mid);

	return tree;
}

static node*
ast_stmt_for(env *ctx) {	
	lex_scan(ctx);

	if (ctx->tok.type != T_LPAREN)
		log_err(L"«դեպի» ցիկլը պետք է սկսվի «(»֊ով");
	lex_scan(ctx);

	node *left;
	left = ast_stmt_single(ctx);

	if (ctx->tok.type != T_SEMI)
		log_err(L"արտահայտությունը պետք է ավարտվի «;»֊ով");
	lex_scan(ctx);

	node *cond;
	cond = ast_expr(ctx, 0);

	node *mid;
	mid = ast_node_init(A_BOOL, cond->prim, 0xBAD);
	ast_node_add(mid, cond);

	if (ctx->tok.type != T_SEMI)
		log_err(L"արտահայտությունը պետք է ավարտվի «;»֊ով");
	lex_scan(ctx);

	node *right;
	right = ast_stmt_single(ctx);

	if (ctx->tok.type != T_RPAREN)
		log_err(L"«դեպի» ցիկլը պետք է ավարտվի «)»֊ով");
	lex_scan(ctx);

	node *block;
	block = ast_stmt_block(ctx);

	node *tree;
	tree = ast_node_init(A_FOR, P_NONE, 0xBAD);
	ast_node_add(tree, left);
	ast_node_add(tree, mid);
	ast_node_add(tree, right);
	ast_node_add(tree, block);

	return tree;
}

static node*
ast_stmt_ret(env *ctx) {
	if (ctx->g_ptype[ctx->f_id] == P_VOID)
		log_err(L"«դատարկ» տիպ վերդարձնող ֆունկցիան չի կարող օգտագործել «տուր» բառը");
	lex_scan(ctx);
	
	node *expr;
	expr = ast_expr(ctx, 0);	

	node *tree;
	tree = ast_node_init(A_RET, P_NONE, ctx->f_id);
	ast_node_add(tree, expr);

	return tree;
}

static node*
ast_stmt_var_decl(env *ctx, p_type prim, c_type stor) {
	p_type  orig;
	orig = prim;

	u32 	id;
	s_type	stc;
	u32	qty;

	node *tree;
	if (stor != C_PARAM)
		tree = ast_node_init(A_GROUP, P_NONE, 0xBAD);

	node *item;
	while (true) {
		if (ctx->tok.type == T_LSQUARE) {
			stc  = S_ARR;
			prim = ast_to_ptr(orig);
			lex_scan(ctx);

			if (ctx->tok.type == T_INT) {
				qty = ctx->tok.val;

				lex_scan(ctx);
			} else {
				qty = 0;
			}

			if (ctx->tok.type != T_RSQUARE)
				log_err(L"զանգվածը սահմանելուց, վերջում պետք է դնել «]»");			
			lex_scan(ctx);
		} else {
			qty  = 1;
			stc  = S_VAR;			 
			prim = orig;
		}

		id = asm_item_add(ctx, prim, stc, qty, stor);	

		if (stor == C_PARAM) {
			tree = ast_node_init(A_VAR, prim, id);			
			break;
		}

		item = ast_node_init(A_VAR, prim, id);
		ast_node_add(tree, item);

		if (ctx->tok.type == T_SEMI) {
			lex_scan(ctx);
			break;
		}

		if (ctx->tok.type == T_COMMA) {
			lex_scan(ctx);
			if (ctx->tok.type != T_IDENT)
				log_err(L"փոփոխականը սահմանելուց պետք է նշել նրա անունը");
			lex_scan(ctx);

			continue;
		}

		log_err(L"միայն կարելի է սահմանել փոփոխականներ և ֆունկցիաներ");
	}

	return tree;
}

static node*
ast_stmt_func_decl(env *ctx, p_type prim) {
	u32 id;
	id = asm_item_find(ctx, C_GLOBAL);
	ctx->f_id = id;

	lex_scan(ctx);

	node *params;
	params = ast_node_init(A_GROUP, P_NONE, 0xBAD);

	node *param;	
	while (ctx->tok.type != T_RPAREN) {
		p_type ptype;
		ptype = ast_prim_type(ctx);

		lex_scan(ctx);

		param = ast_stmt_var_decl(ctx, ptype, C_PARAM);
		ast_node_add(params, param);

		if (ctx->tok.type == T_COMMA)
			lex_scan(ctx);
		else if (ctx->tok.type == T_RPAREN)
			break;
		else
			log_err(L"չճանաչված նշան ֆունկցիան սահմանելիս");
	}

	ctx->g_els[id] = params->len;
	
	if (ctx->tok.type != T_RPAREN)
		log_err(L"ֆունկցիանի անունի վերջում պետք է դնել «)»");
	lex_scan(ctx);		

	node *block;
	block = ast_stmt_block(ctx);

	node *tree;

	if (params != NULL) {
		tree = ast_node_init(A_FUNC, prim, id);
		ast_node_add(tree, params);
		ast_node_add(tree, block);
	} else {
		tree = ast_node_init(A_FUNC, prim, id);
		ast_node_add(tree, block);
	}

	return tree;
}

static node*
ast_stmt_single(env *ctx) {
	node *root;

	switch (ctx->tok.type) {
		case T_U8:
		case T_U16:
		case T_U32:
		case T_U64:
		case T_S8:
		case T_S16:
		case T_S32:
		case T_S64:
		{
			p_type prim;
			prim = ast_prim_type(ctx);

			lex_scan(ctx);	
			root = ast_stmt_var_decl(ctx, prim, C_LOCAL);
			break;
		}
		case T_IF:
			root = ast_stmt_if(ctx);
			break;
		case T_WHILE:
			root = ast_stmt_while(ctx);
			break;
		case T_FOR:
			root = ast_stmt_for(ctx);
			break;
		case T_RET:
			root = ast_stmt_ret(ctx);
			break;
		default:
			root = ast_expr(ctx, 0);
			break;
	}

	return root;
}

static node*
ast_stmt_block(env *ctx) {
	if (ctx->tok.type != T_LBRACE)
		log_err(L"բլոկը պետք է սկսվի «{»-ով");
	lex_scan(ctx);


	node *root;
	root = ast_node_init(A_GROUP, P_NONE, 0xBAD);

	node *block;
	while (true) {
		block = ast_stmt_single(ctx);

		if (block->type == A_CALL ||
		    block->type == A_RET  ||
		    block->type == A_ASSIGN) {
			if (ctx->tok.type != T_SEMI)
				log_err(L"գործողության վերջում պետք է լինի «;» (կետ ստորակետ)");
			lex_scan(ctx);			
		}	

		ast_node_add(root, block);

		if (ctx->tok.type == T_RBRACE) {
			lex_scan(ctx);
			return root;
		}
	}
}

static void
ast_get_ids(env *ctx) {
	while (true) {
		p_type prim;
		prim = ast_prim_type(ctx);

		if (ctx->tok.type != T_IDENT) 
			log_err(L"ֆունկցիան կամ փոփոխականը սահմանելուց պետք է նշել նրա անունը");
		lex_scan(ctx);

		switch (ctx->tok.type) {
		case T_LPAREN:
			asm_item_add(ctx, prim, S_FUNC, 0xBAD, C_GLOBAL);

			while (ctx->tok.type != T_LBRACE)
				lex_scan(ctx);

			u32 cnt;
			cnt = 1;

			while (cnt != 0) {
				lex_scan(ctx);
				
				if (ctx->tok.type == T_LBRACE) {
					++cnt;
					continue;
				}

				if (ctx->tok.type == T_RBRACE) {
					--cnt;
					continue;
				}

				if (ctx->tok.type == T_EOF)
					break;
			}

			break;
		case T_LSQUARE:
			asm_item_add(ctx, prim, S_ARR, 0xBAD, C_GLOBAL);

			do {
				lex_scan(ctx);
			} while(ctx->tok.type != T_SEMI && ctx->tok.type != T_EOF);
			break;
		case T_SEMI:
			asm_item_add(ctx, prim, S_VAR, 0xBAD, C_GLOBAL);
			break;
		default:
			log_err(L"գլոբալ տիրույթում կարելի է սահմանել միայն փոփոխականներ և ֆունկցիաներ");
			break;
		}

		lex_scan(ctx);

		if (ctx->tok.type == T_EOF)
			break;
	}	

	rewind(ctx->in);
	lex_scan(ctx);
}

void
ast_init(env *ctx, char *path) {
	setlocale(LC_ALL, "");

	ctx->rej.type = T_NONE;
	ctx->rej.val  = 0xBAD;
	
	ctx->w_max = 256;
	ctx->word  = malloc(sizeof (s32) * ctx->w_max);

	ctx->put  = 0;
	ctx->line = 0;

	ctx->in = fopen(path, "r");
	if (ctx->in == NULL)
		log_err(L"չստավեց բացել ֆայլը");

#ifdef Z0_DEBUG
	log_info(L"ստեղծվեց «ast»֊ն");
#endif
}

void
ast_read(env *ctx) {
	lex_scan(ctx);

	ast_get_ids(ctx);

	ctx->root = ast_node_init(A_GROUP, P_NONE, 0xBAD);

	node *item;
	while (true) {
		p_type prim;
		prim = ast_prim_type(ctx);

		if (ctx->tok.type != T_IDENT)
			log_err(L"ֆունկցիան կամ փոփոխականը սահմանելուց պետք է նշել նրա անունը");
		lex_scan(ctx);

		if (ctx->tok.type == T_LPAREN)
			item = ast_stmt_func_decl(ctx, prim);
		else 
			item = ast_stmt_var_decl(ctx, prim, C_GLOBAL);
		
		ast_node_add(ctx->root, item);

		if (ctx->tok.type == T_EOF)
			break;
	}

#ifdef Z0_DEBUG
	log_info_tree(ctx->root);
#endif
}

static void
ast_node_free(node *root) {
	for (u32 i = 0; i < root->len; ++i)
		ast_node_free(root->data[i]);
	
	free(root->data);
	free(root);
}

void
ast_free(env *ctx) {
	ast_node_free(ctx->root);
	free(ctx->word);
	fclose(ctx->in);

#ifdef Z0_DEBUG
	log_info(L"ոչնչացվեց «ast»֊ն");
#endif
}
