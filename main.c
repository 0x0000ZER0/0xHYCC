#include "defs.h"

int
main(int argc, char **argv) {
	if (argc < 2) 
		log_err(L"պետք է նշել մուտքային ֆայլերը");

	env ctx;

	ast_init(&ctx, argv[1]);
	asm_init(&ctx);	

	ast_read(&ctx);
	asm_gen(&ctx);

	asm_free(&ctx);
	ast_free(&ctx);
	return 0;
}
