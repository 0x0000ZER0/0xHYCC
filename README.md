# 0xHYCC

The compiler for a `C` like language called `hay`. It is basically a `C` (at least it tries to be one) with *armenian* letters. Because the probablity of "YOU as non-armenian being interested in this language" is close to zer0, I would suggest you to switch the armenian [README](./README.am.md).

(Also thanks to **Warren Toomey** for putting his [journey](https://github.com/DoctorWkt/acwj) of writing a compiler)
