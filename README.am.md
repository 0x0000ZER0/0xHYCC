# 0xHYCC

Հայատառ `C`-անման լեզվի կոմպիլատոր։ Այս կոմպիլատորը գրված է «լրիվ» ([ELF](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format) ֆայլերը դեռ կառուցվում են `GCC`-ի շնորհիվ) զրոյից։

**Ցանկ**

[[_TOC_]]

## Ուղղումներ, հարցեր և առաջարկներ

Ցանկացած ուղղում (լինի ուղղագրական թե տեխնիկական), հարց կամ առաջարկ կարելի է գրել [issues](https://gitlab.com/0x0000ZER0/0xHYCC/-/issues)-ում։

## Ֆիզիկական սահմանափակումները

Այս կոմպիլատորը կառցվում և փորձարկվում է՝

- Linux օպերացիոն համակրգի վրա։
- Աշխատում է միայն `x86_64` (amd64) ԿՄՀ֊ի («CPU»-ի) վրա։ 

## Ֆայլերի վերջավորությունը

Կոմպիլատորը չի ստուգում ֆայլերրի վերջավորությունները, չնայած նրան որ *test*-ում բոլոր ֆայելրը `.հյ` վերջավորությունը ունեն։ Իրականում ցանկացած վերջավորությունով ֆայլ որը պարունակում է լեզվի ճիշտ ուղղագրությունը կարող է թարգմանվել ասսեմբլեր լեզվի։

## Ուղղագրությունը

Այս լեզվի ուղղագրությունը շատ է նման `C` լեզվի ուղղագրությանը։

### Ամենափոքր օրինակը

Քանի որ ասսեմբլերը պարտադրում է սկզբնական կետ ունենալ դրա համար այս լեզու կիրառողը պարտավոր է ունենալ `գլխավոր` ֆունկցիան։

```c
դատարկ
գլխավոր() {

}
```

### Տիպերը

Այս պահին կոմպիլատորը ընդունում է միայն հետևյալ տիպերը՝

|տիպ|չափ (բայթերով) |միջակայքը (նշանակելով փոփոխականը «x»-ով)                    |
|---|---------------|------------------------------------------------------------|
|ա8 |1              |$`0 \leqslant x \leqslant 2^{8}  - 1, \quad x \in \N_0`$    |
|ա16|2              |$`0 \leqslant x \leqslant 2^{16} - 1, \quad x \in \N_0`$    |
|ա32|4              |$`0 \leqslant x \leqslant 2^{32} - 1, \quad x \in \N_0`$    |
|ա64|8              |$`0 \leqslant x \leqslant 2^{64} - 1, \quad x \in \N_0`$    |
|ն8 |1              |$`-2^{7}  \leqslant x \leqslant 2^{7}  - 1, \quad x \in \Z`$|
|ն16|2              |$`-2^{15} \leqslant x \leqslant 2^{15} - 1, \quad x \in \Z`$|
|ն32|4              |$`-2^{31} \leqslant x \leqslant 2^{31} - 1, \quad x \in \Z`$|
|ն64|8              |$`-2^{63} \leqslant x \leqslant 2^{63} - 1, \quad x \in \Z`$|

Վերցնենք «ն32»֊ը որպես օրինակ և հասկանանք նրա անվան իմաստը՝

- «ն» տառը գրվում է սկզբում ցույց տալով որ փոփոխականի սկզբի բիթը օգտագործվում է բացասականը դրականից տարբերելու համար։ «ն» տառը «նշանով» փոփոխականի կարճեցված տարբերակն է։ «ա»֊ն «առանց նշանի» կամ «աննշան» բառի կարճ տարբերակն է, որտեղ փոփոխականի սկզբի բիթը օգտագործվում է միջակայքը ավելի մեծացնելու համար։
- «ն»֊ն կամ «ա» տառին հաջորդող թվերը ցույց են տալիս տիպի չափը բիթերով։ Օրինակ «ն32» տիպի փոփոխական սահմանելիս մարդ կարող է ասել որ հիշողության մեջ հատկացվել են *32* բիթ կամ *4* բայթ տարածք։

### Փոփոխականները

Փոփոխականներ սահմանելիս պարադիր է նշել նրա տիպը և անունը։ 

Օրինակ՝

```
ն32 ա;
ա16 բ;

...
```

Եթե կարիք կա սահմանելու նույն տիպի փոփոխականներ, ապա կարելի է նաև օգտագործել ստորակետը անունների մեջտեղում։

Օրինակ՝

```
ն8 ա, բ, գ, դ, ե;

...
```

փոփոխականների կարելի է վերագրել արժեք հետևյալ կերպ՝

```
ն64 ա;

դատարկ
գլխավոր() {
        ա = 2;
}
```

-  վերևում բերված օրինակում փոփոխականը սահմանված է **գլոբալ** բլոկում՝ հասանելի է ցանկացած վայրից։ **Լոկալ** բլոկում փոփոխական ստեղծելու համար անհրժեշտ է փոփոխականը սահմանել ֆունկցիաի ներսում, ինչպես ցույց է տրված ներգևի օրինակում։

```
դատարկ
գլխավոր() {
        ն64 ա;

        ա = 2;
}
```

### Օպերատորները

Այս պահին կոմպիլատորը կարող է ճանաչել միայն հետևյալ օպերատորները՝

|օպերատոր|կարևորությունը|նկարագրությունը    |օրինակ      |
|--------|--------------|-------------------|------------|
|!       |13            |տրամաբանական ժխտում|պ = **!** ա;     |  
|~       |13            |բինար ժխտում       |պ = **~** ա;     |
|++      |12            |գումարում 1-ով     |պ = **++** ա;    |  
|--      |12            |հանում 1-ով        |պ = ա **--**;    |
|\*      |11            |բազմապատկում       |պ = 2 **\*** 3; |
|/       |11            |բաժանում           |պ = 10 **/** 2; |
|+       |10            |գումարում          |պ = 1 **+** 1;  |
|-       |10            |հանում             |պ = **-** 3;     |
|>>      |9             |աջ տեղափոխություն  |պ = ա **<<** 2; |
|<<      |9             |ձախ տեղափոխություն |պ = ա **>>** 3; |
|>=      |8             |մեծ կամ հավասար    |պ = 3 **>=** 0; |
|<=      |8             |փոքր կամ հավասար   |պ = 2 **<=** 2; |
|>       |8             |մեծ                |պ = 2 **>** 1;  |
|<       |8             |փոքր               |պ = 1 **<** 2;  |
|==      |7             |հավասար            |պ = 9 **==** 9; |
|!=      |7             |անհավասար          |պ = 8 **!=** 9; |
|&       |6             |բինար և            |պ = ա **&** 1;  |
|^       |5             |բացառող կամ        |պ = ա **^** ա;  |
|\|      |4             |բինար կամ          |պ = 0 **\|** ա; |
|և       |3             |տրամաբանական և     |պ = 1 **և** ա;  |
|կամ     |2             |տրամաբանական կամ   |պ = 0 **կամ** ա;|
|=       |1             |վերագրում          |պ **=** 2;      |

### Ճյուղավորում

Տրամաբանական ճյուղավորումներ կառուցելու համար պետք է օգտագործել `եթե` և `այլ` բառերը։

```
եթե (1 == 1) {
        ...
} այլ {
        ...
}
```

### Ցիկլեր 

Լեզվում կան երկու տեսակի ցիկլեր՝

- `մինչդեռ` ցիկլը՝

```
ն32 ի;
ի = 0;

մինչդեռ (ի < 5) {
        տպիր(ի + 1);
        ի = ի + 1;
}
```

վերևում բերված օրինակը կտպի *1, 2, 3, 4, 5*:

- `դեպի` ցիկլը՝

```
ն32 ի;

դեպի (ի = 5; ի > 0; ի = ի - 1) {
        տպիր(ի);
}
```

վերևում բերված օրինակը կտպի *5, 4, 3, 2, 1*:

### Ցուցիչներ և Հասցեներ

Դիտարկենք հետևյալ օրինակը՝

```
ն32  ա;
ն32 *ց;

դատարկ
գլխավոր() {
        ա = 19;
        ց = &ա;

        տպիր(*ց);
}
```

- առաջին տողում սահմավում է `ն32` տիպի `ա` անունով փոփոխականը։
- երկրորդ տողում սահմանվում է ցուցիչ որը ընդունում է միայն `ն32` տիպի հասցեներ։ Ցուցիչ սահմանելիս պարտադիր է ունենալ `*` նշանը, տիպի և անվան միջև։
- յոթերորդ տողում `ա`-ին տրվում է `19` արժեքը։
- հաջորդ տողում `ց` ցուցիչին վերագրվում է `ա`-ի հասցեն։ Հասցեն ստանալու համար պարադիր է ունենալ `&` նշանը անունից առաջ։
- հասցեն պահելուց հետո վերջին տողում տպվում է ցուցիչի տվյալ հասցեում պահված արժեքը։ 

### Մեկնաբանություններ

Մեկնաբանությունները անտեսվում են կոմպիլատորի կողմից։ Մեկնաբանությունների տեսակները երկուսն են՝

- *մեկ տողանի*՝ այս տիպի մեկնաբանությունները կարելի է ստեղծել նախադասության սկզբում դնելով `//` նշանը։

```
// բացատրություն
ն32 ա;
ա = (բ + 31) & ~31; 
```

- *մի քանի տողանի*՝ այս տիպի մեկնաբանությունները սահմանափակվում են `/*` և `*/` նշանների միջև։


```
/*
        բացատրություն
        բացատրություն
        բացատրություն
*/
ն32 ա;

պ = (ա ^ ա) == 0; 
```

### Ֆունկցիաներ

Նույն կոդը մի քանի անգամ չգրելու համար կարելի է օգտագործել ֆունկցիաներ։ Դիտարկենք հետևյալ օրինակը՝

```
...

պ = ա + բ + գ;
պ = (պ և ա) / զ + *ի; 
տպիր(պ);

...

պ = ա + բ + գ;
պ = (պ և ա) / զ + *ի; 
տպիր(պ);

...
```

Ինչպես տեսնում ենք, ունենք կոդ որը կարելի է «փոքրացնել»: Այս օրինակի համար կարելի է ստեղծել մեկ ֆունկցիա առանց պարամետրերի՝

```
դատարկ
ֆունկցիա() {
        պ = ա + բ + գ;
        պ = (պ և ա) / զ + *ի; 
        տպիր(պ);
}
```

ֆունկցիան կարուցելուց հետո կարելի է այն հեշտորեն օգտագործել հետևյալ կերպ՝

```
...
ֆունկցիա();
...
ֆունկցիա();
...
```

Բերված օրինակում ենթադրվում էին երկու կետեր։
- ֆունկցիան չպետք է վերադարձնի ստացված արժեքը։
- բոլոր օգտագործվող փոփոխականները **գլոբալ** տիրույթում են պահված։

Եթե անհրաժեշտ է վերադարձնել ինչ որ արժեք, ապա պետք է գրել ֆունկցիաի վերադարձնող տիպը սկզբում։ Այնուհետև պետք է նշել թե ինչն է պետք վերադարձնել։ Ենթադրենք որ պետք է վերադարձնել վերևում բերված օրինակի `պ` փոփոխականի արժեքը։

```
ն32
ֆունկցիա() {
        պ = ա + բ + գ;
        պ = (պ և ա) / զ + *ի; 

        տուր պ;
}
```

- ամենասկզբում նշված է ֆունկցիաի վերադարձվող արժեքի տիպը՝ `ն32`:
- հաշվարկները ավարտելուց հետո վերադարձվում է պահանջվող արժեքը՝ `տուր` բառի շնորհիվ։

Վերադարձնող ֆունկցիաները կարելի է օգտագործել հետևյալ կերպ՝

```
...
պ = ֆունկցիա();
...
```

Այս օրինակում `պ`-ին վերագրվում է `ֆունկցիա`-ի վերադարձվող արժեքը։

Փորձենք ֆունկցիան ավելի ընդհանրացնել։ Նախորդ օրինակներում ենթադրվում էր որ արդեն գություն ունեն **գլոբալ** տիրույթում սահմանված փոփոխականներ։ Ֆունկցիաներներ կառուցելիս կարելի է նշել թե ինչ *արգումենտներ* կարող է ընդունել տրված ֆունկցիան։

```
ն32
ֆունկցիա(ն32 ա, ն32 բ, ն32 գ, ն32 զ, ն32 *ի) {
        ն32 պ;

        պ = ա + բ + գ;
        պ = (պ և ա) / զ + *ի; 

        տուր պ;
}
```

Այս դեպքում ֆունկցիան ավելի ընդհանրացված է քանի որ պետք չէ ենթադրել որ գություն ունեն արդեն սահմանված օգտագործվող անուններով փոփոխականներ։

- ֆունկցիաի անունից հետո նշված են ընդունվող պարամետրերը։ Այս դեպքում՝ `ա`-ն, `բ`-ն, `գ`-ն, `զ`-ն և `ի`-ն։ Պարամետրերի անուններից առաջ պարտադիր է նշել տվյալ պարամետրի տիպը։ Օրինակ ինչպես տեսնում ենք,  `ա`-ի, `բ`-ի, `գ`-ի և `զ`-ի տիպն է `ն32`, բայց `ի`-ի համար նշված է `ն32*` տիպը։

Պարամետրերով ֆունկցիան կարելի է օգտագործել հետևյալ կերպ՝

```
ֆունկցիա(1, -3, 2 * 3, ա, &դ);
```

Այս օրինակում ֆունկցիան կանչելիս տրված են՝ `1`-ը, `-3`-ը, `2 * 3`-ը, `ա`-ն և `դ`-ի հասցեն ստորակետերով անջատված։

## Գործիքներ

- *VIM* օգտագործողների համար առկա է՝ syntax ֆայլը որը կարելի է ստանալ հետևյալ [հղումով](https://gitlab.com/0x0000ZER0/0xHYCC/-/blob/master/tools/hy.vim)։
- եթե (անհասկանալի պատճառներով) պետք է Linux-ում Հայերեն ստեղնաշարի դասավորությունը, ապա կարելի է այն վերցնել հետևյալ [հղումից](https://gitlab.com/0x0000ZER0/0xHYCC/-/blob/master/tools/am)։
