#include "defs.h"

#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>

static void
log_t_type(t_type type) {
	switch (type) {
	case T_PLUS:		
		fwprintf(stdout, L"T_PLUS");
		break;
	case T_MINUS:
		fwprintf(stdout, L"T_MINUS");
		break;
	case T_STAR:
		fwprintf(stdout, L"T_STAR");
		break;
	case T_SLASH:
		fwprintf(stdout, L"T_SLASH");
		break;
	case T_INT:
		fwprintf(stdout, L"T_INT");
		break;
	case T_STR:
		fwprintf(stdout, L"T_STR");
		break;
	case T_EQ:
		fwprintf(stdout, L"T_EQ");
		break;
	case T_NE:
		fwprintf(stdout, L"T_NE");
		break;
	case T_LT:
		fwprintf(stdout, L"T_LT");
		break;
	case T_GT:
		fwprintf(stdout, L"T_GT");
		break;
	case T_LE:
		fwprintf(stdout, L"T_LE");
		break;
	case T_GE:
		fwprintf(stdout, L"T_GE");
		break;
	case T_EOF:
		fwprintf(stdout, L"T_EOF");
		break;
	case T_SEMI:
		fwprintf(stdout, L"T_SEMI");
		break;
	case T_COMMA:
		fwprintf(stdout, L"T_COMMA");
		break;
	case T_ASSIGN:
		fwprintf(stdout, L"T_ASSIGN");
		break;
	case T_U8:
		fwprintf(stdout, L"T_U8»");
		break;
	case T_U16:
		fwprintf(stdout, L"T_U16");
		break;
	case T_U32:
		fwprintf(stdout, L"T_U32");
		break;
	case T_U64:
		fwprintf(stdout, L"T_U64");
		break;
	case T_S8:
		fwprintf(stdout, L"T_S8");
		break;
	case T_S16:
		fwprintf(stdout, L"T_S16");
		break;
	case T_S32:
		fwprintf(stdout, L"T_S32");
		break;
	case T_S64:
		fwprintf(stdout, L"T_S64");
		break;
	case T_VOID:
		fwprintf(stdout, L"T_VOID");
		break;
	case T_IDENT:
		fwprintf(stdout, L"T_IDENT");
		break;
	case T_LPAREN:
		fwprintf(stdout, L"T_LPAREN");
		break;
	case T_RPAREN:
		fwprintf(stdout, L"T_RPAREN");
		break;
	case T_LBRACE:
		fwprintf(stdout, L"T_LBRACE");
		break;
	case T_RBRACE:
		fwprintf(stdout, L"T_RBRACE");
		break;
	case T_LSQUARE:
		fwprintf(stdout, L"T_LSQUARE");
		break;
	case T_RSQUARE:
		fwprintf(stdout, L"T_RSQUARE");
		break;
	case T_IF:
		fwprintf(stdout, L"T_IF");
		break;
	case T_ELSE:
		fwprintf(stdout, L"T_ELSE");
		break;
	case T_WHILE:
		fwprintf(stdout, L"T_WHILE");
		break;
	case T_FOR:
		fwprintf(stdout, L"T_FOR");
		break;
	case T_RET:
		fwprintf(stdout, L"T_RET");
		break;
	case T_AMPER:
		fwprintf(stdout, L"T_AMPER");
		break;
	case T_L_AND:
		fwprintf(stdout, L"T_L_AND");
		break;
	case T_L_OR:
		fwprintf(stdout, L"T_L_OR");
		break;
	case T_L_NOT:
		fwprintf(stdout, L"T_L_NOT");
		break;
	case T_B_OR:
	       	fwprintf(stdout, L"T_B_OR");
	       	break;
	case T_B_XOR:
	       	fwprintf(stdout, L"T_B_XOR");
	       	break;
	case T_B_INV:
		fwprintf(stdout, L"T_B_INV");
		break;
	case T_B_LSH:
		fwprintf(stdout, L"T_B_LSH");
		break;
	case T_B_RSH:
		fwprintf(stdout, L"T_B_RSH");
		break;
	case T_INC:
	       	fwprintf(stdout, L"T_INC");
	       	break;
	case T_DEC:
		fwprintf(stdout, L"T_DEC");
		break;
	case T_NONE:  
		fwprintf(stdout, L"T_NONE");
		break;
	}
}

static void
log_a_type(a_type type) {
	switch (type) {
	case A_ADD:
		fwprintf(stdout, L"A_ADD");
		break;
	case A_SUB:
		fwprintf(stdout, L"A_SUB");
		break;
	case A_MUL:
		fwprintf(stdout, L"A_MUL");
		break;
	case A_DIV:
		fwprintf(stdout, L"A_DIV");
		break;
	case A_INT:
		fwprintf(stdout, L"A_INT");
		break;
	case A_STR:
		fwprintf(stdout, L"A_STR");
		break;
	case A_EQ:
		fwprintf(stdout, L"A_EQ");
		break;
	case A_NE:
		fwprintf(stdout, L"A_NE");
		break;
	case A_LT:
		fwprintf(stdout, L"A_LT");
		break;
	case A_GT:
		fwprintf(stdout, L"A_GT");
		break;
	case A_LE:
		fwprintf(stdout, L"A_LE");
		break;
	case A_GE:
		fwprintf(stdout, L"A_GE");
		break;
	case A_IDENT:
		fwprintf(stdout, L"A_IDENT");
		break;
	case A_ASSIGN:
		fwprintf(stdout, L"A_ASSIGN");
		break;
	case A_VAR:
		fwprintf(stdout, L"A_VAR");
		break;
	case A_IF:
		fwprintf(stdout, L"A_IF");
		break;
	case A_WHILE:
		fwprintf(stdout, L"A_WHILE");
		break;
	case A_FOR:
		fwprintf(stdout, L"A_FOR");
		break;
	case A_FUNC:
		fwprintf(stdout, L"A_FUNC");
		break;
	case A_CALL:
		fwprintf(stdout, L"A_CALL");
		break;
	case A_RET:
		fwprintf(stdout, L"A_RET");
		break;
	case A_ADDR:
		fwprintf(stdout, L"A_ADDR");
		break;
	case A_DREF:
		fwprintf(stdout, L"A_DREF");
		break;
	case A_SCALE:
		fwprintf(stdout, L"A_SCALE");
		break;
	case A_L_AND:
		fwprintf(stdout, L"A_L_AND");
		break;
	case A_L_OR:
		fwprintf(stdout, L"A_L_OR");
		break;
	case A_L_NOT:
		fwprintf(stdout, L"A_L_NOT");
		break;
	case A_B_AND:
	       	fwprintf(stdout, L"A_B_AND");
	       	break;
	case A_B_OR:
	       	fwprintf(stdout, L"A_B_OR");
	       	break;
	case A_B_XOR:
	       	fwprintf(stdout, L"A_B_XOR");
	       	break;
	case A_B_INV:
		fwprintf(stdout, L"A_B_INV");
		break;
	case A_B_LSH:
		fwprintf(stdout, L"A_B_LSH");
		break;
	case A_B_RSH:
		fwprintf(stdout, L"A_B_RSH");
		break;
	case A_PRE_INC:
	       	fwprintf(stdout, L"A_PRE_INC");
	       	break;
	case A_PRE_DEC:
		fwprintf(stdout, L"A_PRE_DEC");
		break;
	case A_POST_INC:
	       	fwprintf(stdout, L"A_POST_INC");
	       	break;
	case A_POST_DEC:
		fwprintf(stdout, L"A_POST_DEC");
		break;
	case A_BOOL:
	       	fwprintf(stdout, L"A_BOOL");
	       	break;
	case A_NEG:
		fwprintf(stdout, L"A_NEG");
		break;
	case A_GROUP:
		fwprintf(stdout, L"A_GROUP");
		break;
	}
}

static void
log_p_type(p_type type) {
	switch (type) {
	case P_NONE:		
		fwprintf(stdout, L"P_NONE");
		break;
	case P_VOID:		
		fwprintf(stdout, L"P_VOID");
		break;
	case P_U8:		
		fwprintf(stdout, L"P_U8");
		break;
	case P_U16:		
		fwprintf(stdout, L"P_U16");
		break;
	case P_U32:		
		fwprintf(stdout, L"P_U32");
		break;
	case P_U64:		
		fwprintf(stdout, L"P_U64");
		break;
	case P_S8:		
		fwprintf(stdout, L"P_S8");
		break;
	case P_S16:		
		fwprintf(stdout, L"P_S16");
		break;
	case P_S32:		
		fwprintf(stdout, L"P_S32");
		break;
	case P_S64:		
		fwprintf(stdout, L"P_S64");
		break;
	case P_VOID_PTR:	   
		fwprintf(stdout, L"P_VOID_PTR");
		break;
	case P_U8_PTR:		
		fwprintf(stdout, L"P_U8_PTR");
		break;
	case P_U16_PTR:		
		fwprintf(stdout, L"P_U16_PTR");
		break;
	case P_U32_PTR:		
		fwprintf(stdout, L"P_U32_PTR");
		break;
	case P_U64_PTR:		
		fwprintf(stdout, L"P_U64_PTR");
		break;
	case P_S8_PTR:		
		fwprintf(stdout, L"P_S8_PTR");
		break;
	case P_S16_PTR:		
		fwprintf(stdout, L"P_S16_PTR");
		break;
	case P_S32_PTR:		
		fwprintf(stdout, L"P_S32_PTR");
		break;
	case P_S64_PTR:		
		fwprintf(stdout, L"P_S64_PTR");
		break;
	}
}

static void
log_c_type(c_type type) {
	switch (type) {
	case C_LOCAL:
		fwprintf(stdout, L"C_LOCAL");
		break;
	case C_PARAM:
		fwprintf(stdout, L"C_PARAM");
		break;
	case C_GLOBAL:
		fwprintf(stdout, L"C_GLOBAL");
		break;
	}
}

void
log_err(s32 *txt) {
	fwprintf(stderr, L"ՍԽԱԼ՝ %ls։\n", txt);

	exit(1);
}

void
log_warn(s32 *txt) {
	fwprintf(stderr, L"ԶԳՈՒՇԱՑՈՒՄ՝ %ls։\n", txt);
}

void
log_info(s32 *txt) {
	fwprintf(stdout, L"ՏԵՂԵԿՈՒԹՅՈՒՆ՝ %ls:\n", txt);
}

void
log_info_u32(s32 *txt, u32 num) {
	fwprintf(stdout, L"ՏԵՂԵԿՈՒԹՅՈՒՆ՝ %ls, ԹԻՎ՝ «%u»:\n", txt, num);
}

void
log_info_tok(s32 *txt, token tok) {
	fwprintf(stdout, L"ՏԵՂԵԿՈՒԹՅՈՒՆ՝ %ls, ", txt);
	fwprintf(stdout, L"ՆՇԱՆ՝ «");
	log_t_type(tok.type);
	fwprintf(stdout, L"»");

	if (tok.type == T_INT)
		fwprintf(stdout, L", ԱՐԺԵՔԸ՝ «%i»", tok.val);
	
	fwprintf(stdout, L":\n");
}

static void
log_info_node(node *root, u32 tabs) {
	for (u32 i = 0; i < tabs; ++i)
		fwprintf(stdout, L"    ");

	fwprintf(stdout, L"ՏԻՊ՝ «");
	log_a_type(root->type);
	fwprintf(stdout, L"»");

	switch (root->type) {
	case A_INT:
		fwprintf(stdout, L", ԱՐժԵՔԸ՝ «%i»", root->val);	
		break;
	case A_STR:
	case A_IDENT:
	case A_CALL:
		fwprintf(stdout, L", ԻՆԴԵՔՍԸ՝ «%u»", root->id);	
		break;
	case A_SCALE:
		fwprintf(stdout, L", ՉԱՓԸ՝ «%u»", asm_type_size(root->prim));
		break;
	default:
		break;
	}
	
	if (root->type == A_ADDR ||
	    root->type == A_DREF ||
	    root->type == A_VAR) {
		fwprintf(stdout, L", «");
		log_p_type(root->prim);
		fwprintf(stdout, L"»");
		
	}

	if (root->lval)
		fwprintf(stdout, L", {ՁԱԽ}");
	else
		fwprintf(stdout, L", {ԱՋ}");
	fwprintf(stdout, L", (%u):\n", root->len);

	for (u32 i = 0; i < root->len; ++i)
		log_info_node(root->data[i], tabs + 1);
}

void
log_info_tree(node *root) {
	u32 tabs;
	tabs = 0;

	log_info_node(root, tabs);
}

void
log_info_prim(s32 *txt, p_type prim) {
	fwprintf(stdout, L"ՏԵՂԵԿՈՒԹՅՈՒՆ՝ %ls, ՀԱՍ՝ «", txt);
	log_p_type(prim);
	fwprintf(stdout, L"»:\n");
}

void
log_info_table(env *ctx) {
	fwprintf(stdout, L"================ ԱՂՅՈՒՍԱԿ (%u) ================\n", ctx->g_len);

	for (u32 i = 0; i < ctx->g_len; ++i) {
		fwprintf(stdout, L"ՏԻՊԸ՝   "); 
		log_p_type(ctx->g_ptype[i]);
		fwprintf(stdout, L"\n");

		fwprintf(stdout, L"ԱՆՈՒՆԸ՝ %ls\n", ctx->g_name[i]);

		fwprintf(stdout, L"ՏԵՍԱԿԸ՝ ");
		log_c_type(ctx->g_ctype[i]);
		fwprintf(stdout, L"\n");

		fwprintf(stdout, L"ԻՆԴԵՔՍ՝ %u\n", i);
		fwprintf(stdout, L"ՔԱՆԱԿԸ՝ %u\n", ctx->g_els[i]);

		if (ctx->g_ctype[i] & (C_PARAM | C_LOCAL))
			fwprintf(stdout, L"ԴԻՐՔԸ՝  %u\n", ctx->g_pos[i]);

		fwprintf(stdout, L"\n");
	}
}
