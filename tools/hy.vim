syn keyword Type դատարկ
syn keyword Type ա8
syn keyword Type ա16
syn keyword Type ա32
syn keyword Type ա64
syn keyword Type ն8
syn keyword Type ն16
syn keyword Type ն32
syn keyword Type ն64
syn keyword Keyword եթե
syn keyword Keyword այլ
syn keyword Keyword մինչդեռ
syn keyword Keyword դեպի
syn keyword Keyword տուր
syn keyword Operator և
syn keyword Operator կամ

syn match Function /\k\+\%(\s*(\)\@=/
syn match Comment /\/\/.*$/
syn region Comment start="/\*" end="\*/"
